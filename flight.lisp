(in-package :skyw0r)

(defresource "doppler-1.wav" :volume 180)
(defresource "doppler-2.wav" :volume 180)
(defresource "doppler-3.wav" :volume 180)

(defparameter *doppler-sounds* '("doppler-1.wav" "doppler-2.wav" "doppler-3.wav"))

(defresource "interference.wav" :volume 70)
(defresource "scanner.wav" :volume 100)
(defresource "error.wav" :volume 150)
(defresource "alarm.wav" :volume 150)
(defresource "xiomacs.ogg" :volume 200)
(defresource "neo-eof2.ogg" :volume 200)
(defresource "xiomacs2.ogg" :volume 200)
(defresource "xiomacs3.ogg" :volume 200)
(defresource "starsaga.ogg" :volume 150)
(defresource "engine.ogg" :volume 35)
(defresource "xiomacs.ogg" :volume 50)
(defresource "cruise.wav" :volume 20)
(defresource "lockon.wav" :volume 80)
(defresource "radar-lock.wav" :volume 100)
(defresource "explode-1.wav" :volume 100)
(defresource "explode-2.wav" :volume 100)
(defresource "thrust.wav" :volume 55)
(defresource "crystal.wav" :volume 155)
(defresource "creep.wav" :volume 80)
(defresource "fire-1.wav" :volume 240)
(defresource "fire-2.wav" :volume 240)
(defresource "fire-3.wav" :volume 240)
(defresource "whack1.wav" :volume 155)
(defresource "whack2.wav" :volume 155)
(defresource "whack3.wav" :volume 155)
(defresource "data-1.wav" :volume 140)
(defresource "data-2.wav" :volume 140)
(defresource "data-3.wav" :volume 140)
(defresource "bump-1.wav" :volume 140)
(defresource "encounter.wav" :volume 140)
(defresource "rezz-1.wav" :volume 80)
(defresource "rezz-2.wav" :volume 80)
(defresource "rezz-3.wav" :volume 80)
(defresource "powerup.wav" :volume 140)
(defresource "powerdown.wav" :volume 80)

(defparameter *star-cluster-images* 
  '("aquastar.png" "bluestar.png" "flarestar.png" "greenstar.png"))

(defvar *engine-channel* nil)

(defparameter *damage-sounds* '("rezz-1.wav" "rezz-2.wav" "rezz-3.wav"))

(defun play-error-sound () 
  (when *use-sound* (play-sample "error.wav")))

(defun play-alarm-sound () 
  (when *use-sound* (play-sample "alarm.wav")))

(defun play-damage-sound ()
  (when *use-sound* (play-sample (random-choose *damage-sounds*))))

(defparameter *whack-sounds* '("whack1.wav" "whack2.wav" "whack3.wav"))

(defun play-whack-sound ()
  (when *use-sound* (play-sample (random-choose *whack-sounds*))))

(defun play-data-sound () 
  (when *use-sound* (play-sample (random-choose '("data-1.wav" "data-2.wav" "data-3.wav")))))

(defun play-doppler-sound ()
  (when *use-sound* (play-sample (random-choose *doppler-sounds*))))

(defun play-explosion-sound ()
  (when *use-sound* (play-sample (random-choose '("explode-1.wav" "explode-2.wav")))))

(defun play-fire-sound () 
  (when *use-sound*
    (play-sample (random-choose '("fire-1.wav" "fire-2.wav" "fire-3.wav")))))

(defun thrust-sound-p () *engine-channel*)

(defun play-thrust-sound ()
  (when *use-sound*
    (when *engine-channel* (halt-sample *engine-channel*))
    (setf *engine-channel* 
	  (play-sample "thrust.wav" :loop t))))

 (defun stop-thrust-sound ()
  (when *engine-channel* (halt-sample *engine-channel*))
  (setf *engine-channel* nil))

(defun stop-engine-sound ()
  (halt-music 10))

(defvar *player-1-joystick* 0)
(defvar *player-2-joystick* 0)

(defparameter *skyw0r-quadtree-depth* 10)
(defparameter *flight-size* 10000)
(defparameter *void-size* 80000)

(defparameter *default-focal-length* 100)
(defparameter *focal-length* 1200)
(defun focal-length () *focal-length*)
(defun set-focal-length (length) (setf *focal-length* length))
(defsetf focal-length set-focal-length)

(defparameter *dead-zone* 0.1)

(defvar *ship* nil)

(defun ship () *ship*)
(defun set-ship (ship) (setf *ship* ship))
(defsetf ship set-ship)

(defun fuel () (field-value 'fuel (ship)))
(defun energy () (field-value 'energy (ship)))
(defun shield () (field-value 'shield (ship)))
(defun altitude () (field-value 'altitude (ship)))

(defvar *joystick-enabled* t)

(defun horizon-delta () (if (ship)
			    (climb-delta (ship))
			    0))

(defun horizon-y () (+ (window-y) (horizon-delta) 
		       (/ *height* 2)))
(defun horizon-x () (+ (window-x) (/ *width* 2)))

(defun vanishing-point ()
  (values (horizon-x) (+ (window-y) 
			 (/ *height* 2)
			 (* 0.5 (horizon-delta)))))

(defun window-vanishing-point ()
  (multiple-value-bind (vx vy) (horizon-point)
    (values (- vx (window-x))
	    (- vy (window-y) (horizon-delta)))))

(defun horizon-point () (values (horizon-x) 
				(- (horizon-y)
				   (horizon-delta))))

(defun texel-vanishing-point () (values (horizon-x)
					(horizon-y)))

(defun window-horizon-point ()
  (multiple-value-bind (vx vy) (horizon-point)
    (values (- vx (window-x))
	    (- vy (window-y)))))

(defun ship-center () (center-point (ship)))
(defun ship-x () (nth-value 0 (ship-center)))
(defun ship-y () (nth-value 1 (ship-center)))
(defun ship-speed () (* 10 (field-value 'dz (ship))))

(defun ship-window-x () (- (ship-x) (window-x)))
(defun ship-window-y () (- (ship-y) (window-y)))

(defun window-top () (window-y))
(defun window-left () (window-x))
(defun window-right () (+ (window-x) *width*))
(defun window-bottom () (+ (window-y) *height*))

(defun flight-width () (field-value 'width (current-buffer)))
(defun flight-height () (field-value 'height (current-buffer)))

(defun wraparound-left () (* 1/3 (flight-width)))
(defun wraparound-right () (* 2/3 (flight-width)))
(defun wraparound-top () (* 1/3 (flight-height)))
(defun wraparound-bottom () (* 2/3 (flight-height)))

(defvar *current-flight* nil)
(defun current-flight () *current-flight*)

(defparameter *max-roll* (radian-angle 85))
(defun roll () (field-value 'heading (ship))) 

(defparameter *flightpath-border-size* 639)
;;(defparameter *flightpath-border-size* 400)

(defun flightpath-left () (+ (window-left) *flightpath-border-size*))
(defun flightpath-right () (- (window-right) *flightpath-border-size*))
(defun flightpath-center () (/ (+ (flightpath-left)
				  (flightpath-right))
			       2))

(defparameter *hitzone-border-size* 490)

(defun hitzone-top () (+ (window-top) 180))
(defun hitzone-left () (+ (window-left) *hitzone-border-size*))
(defun hitzone-right () (- (window-right) *hitzone-border-size*))
(defun hitzone-bottom () (+ (window-top) 470))

(defun hitzone ()
  (values (hitzone-top) (hitzone-left)
	  (hitzone-right) (hitzone-bottom)))

(defun hitzone-p (thing)
  (colliding-with-rectangle-p
   thing 
   (hitzone-top)  
   (hitzone-left)
   (- (hitzone-right) (hitzone-left))
   (- (hitzone-bottom) (hitzone-top))))

(defun dodge-left () (- (hitzone-left) 120))
(defun dodge-right () (+ (hitzone-right) 120))
(defun escape-left () (- (dodge-left) 300))
(defun escape-right () (- (dodge-left) 300))
(defun escape-bottom () (+ (ship-y) 100))

(defparameter *ship-camera-y* 550)
(defparameter *hud-y* 580)
(defun hud-y () (+ (window-top) *hud-y*))

(defun radar-range () (slot-value (ship) 'radar-range))
(defparameter *radar-width* 150)
(defun radar-left () (+ (window-x) (units 1)))
(defun radar-right () (+ (radar-left) *radar-width*))
(defun radar-top () (+ (hud-y) 10))
(defun radar-bottom () (+ *height* (window-top) (units -1)))

;;; Control functions

(defun holding-down-arrow () (or (keyboard-down-p :kp2) (keyboard-down-p :down)))
(defun holding-up-arrow () (or (keyboard-down-p :kp8) (keyboard-down-p :up)))
(defun holding-left-arrow () (or (keyboard-down-p :kp4) (keyboard-down-p :left)))
(defun holding-right-arrow () (or (keyboard-down-p :kp6) (keyboard-down-p :right)))
(defun holding-space () (keyboard-down-p :space))     
(defun holding-return () (or (keyboard-down-p :kp-enter)
			     (keyboard-down-p :return)))

(defun arrow-direction ()
  (cond 
    ((and (holding-down-arrow) (holding-right-arrow)) :downright)
    ((and (holding-down-arrow) (holding-left-arrow)) :downleft)
    ((and (holding-up-arrow) (holding-right-arrow)) :upright)
    ((and (holding-up-arrow) (holding-left-arrow)) :upleft)
    ((holding-down-arrow) :down)
    ((holding-up-arrow) :up)
    ((holding-left-arrow) :left)
    ((holding-right-arrow) :right)))

(defun holding-joystick-fire-button ()
  (some #'(lambda (button)
	    (joystick-button-pressed-p button *player-1-joystick*))
	'(2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)))

(defun holding-fire ()  
  (or (holding-shift)
      (holding-joystick-fire-button)))

(defparameter *menu-button* 0)
(defparameter *alt-fire-button* 1)

(defun holding-tab ()
  (keyboard-down-p :tab))

(defun holding-alt-fire ()
  (or (holding-tab)
      (holding-return)
      (joystick-button-pressed-p *alt-fire-button* *player-1-joystick*)))

(defun holding-joystick-menu-button ()
  (joystick-button-pressed-p *menu-button* *player-1-joystick*))

(defun holding-menu-button ()
  (or (holding-space)
      (holding-joystick-menu-button)))

;;; Sparks

(defparameter *spark-images* '("rezlight1.png" "rezlight2.png" "rezlight3.png" "rezlight4.png" "rezlight5.png"))

(defun spark-size () (random-choose '(16 19 22)))

(defclass spark (texel)
  ((timer :initform (random-choose '(20 30 40 50 60)))
   (speed :initform (random-choose '(6 5 4 3)))
   (image :initform (random-choose *spark-images*))))

(defmethod initialize-instance :after ((spark spark) &key heading)
  (setf (field-value 'heading spark) heading)
  (let ((size (spark-size)))
    (resize spark size size)))

(defmethod update ((spark spark))
  (with-slots (timer speed heading width height) spark
    (decf timer)
    (if (not (plusp timer))
	(destroy spark)
	(progn
	  (move spark heading speed)
	  (incf speed 0.3)
	  (resize spark (- width 0.2) (- height 0.2))))))

(defmethod draw ((spark spark))
  (with-slots (x y width height heading image) spark
    (draw-textured-rectangle-* x y 0 width height (find-texture (random-choose *spark-images*)) :blend :additive)))

(defun make-sparks (x y &optional (count (random-choose '(4 5 6))))
  (dotimes (n count)
    (add-node (current-buffer) (new 'spark :heading (random (* 2 pi))) x y)))

;;; Ion dust

(defparameter *ion-images* '("rezblur1.png" "rezblur2.png" "rezblur3.png" "rezblur4.png" "rezblur5.png"))
(defun ion-size () (random-choose '(10 12 15)))

(defclass ion (texel)
  ((timer :initform (random-choose '(120 130 140 150 160 170 180 190 100)))
   (speed :initform (random-choose '(0.01 0.02 0.03)))
   (image :initform (random-choose *spark-images*))))

(defmethod initialize-instance :after ((ion ion) &key heading)
  (setf (field-value 'heading ion) heading)
  (let ((size (ion-size)))
    (resize ion size size)))

(defmethod update ((ion ion))
  (with-slots (timer speed heading width height) ion
    (decf timer)
    (if (not (plusp timer))
	(destroy ion)
	(progn
	  (move ion heading speed)
	  (incf speed 0.01)))))

(defmethod draw ((ion ion))
  (with-slots (x y width height heading image) ion
    (draw-textured-rectangle-* x y 0 width height (find-texture (random-choose *spark-images*)) :blend :additive)))

(defun make-ions (x y &optional (count (random-choose '(4 5 6))))
  (dotimes (n count)
    (add-node (current-buffer) (new 'ion :heading (random (* 2 pi))) x y)))

;;; Bullets

(defparameter *bullet-speed* 12)
(defparameter *bullet-cannon-heat* 100)
(defparameter *bullet-cannon-cost* 27)
(defparameter *bullet-reload-time* 16)
(defparameter *bullet-life* 21)
(defparameter *bullet-speed* 8)

(defun firing-angle () (roll))

(defun firing-location ()
  (values (- (ship-x) (units 0.25))
	  (- (ship-y) (units 2))))

(defclass bullet (texel)
  ((timer :initform *bullet-life*)
   (image :initform (random-choose '("bullet-1.png" "bullet-2.png")))))

(defmethod initialize-instance :after ((bullet bullet) &key)
  (setf (field-value 'heading bullet) (firing-angle)))

(defmethod update ((bullet bullet))
  (with-slots (timer heading width height) bullet
    (decf timer)
    (if (not (plusp timer))
	(destroy bullet)
	(progn
	  (move bullet heading *bullet-speed*)
	  (resize bullet (- width 0.4) (- height 0.6))))))

(defmethod draw ((bullet bullet))
  (with-slots (x y width height heading image blend) bullet
    (draw-textured-rectangle-* x y 0 width height (find-texture image) :angle (roll)))) 

;;; Missiles

(defclass missile (texel)
  ((timer :initform 400)
   (target :initform nil)
   (image :initform "bomb-flash1.png")))

(defmethod initialize-instance :after ((missile missile) &key target)
  (with-slots (heading) missile
    (let ((enemy (or target (nearest-enemy))))
      (setf (slot-value missile 'target) enemy)
      (resize missile 20 20)
      (setf heading
	    (or (when enemy (heading-between (ship) enemy))
		(firing-angle))))))

(defmethod update ((missile missile))
  (with-slots (target timer heading width height) missile
    (decf timer)
    (if (not (plusp timer))
	(destroy missile)
	(progn
	  (multiple-value-bind (x y) (center-point missile)
	    (percent-of-time 50 (make-ions x y 3)))
	  (move missile (or (when target (heading-between missile target)) heading) 5)))))

(defmethod draw ((missile missile))
  (with-slots (x y width height heading image blend) missile
    (draw-textured-rectangle-* x y 0 width height (find-texture (random-choose '("bomb-flash1.png" "bomb-flash2.png" "bomb-flash3.png"))) :angle (roll))))

;;; The player's ship is the center of the action.

(defparameter *maximum-altitude* 9999)
(defparameter *maximum-energy* 9999)
(defparameter *maximum-fuel* 100.0)
(defparameter *maximum-shield* 9999)
(defparameter *maximum-missiles* 10)
(defparameter *maximum-dx* 12)
(defparameter *maximum-dy* 5)
(defparameter *maximum-ddx* 4)
(defparameter *maximum-ddy* 2)

(defparameter *materials* '(iron carbon cadmium plutonium francium
  neuplextocrom-b neuplextocrom-a technetium einsteinium molybdenum
  xalcium rezrovium xivixium platinum endurium diamond extropium
  crystalphex biosilicate feltronite dioxazine
  unidentified-fractal-form))

(defun random-material ()
  (random-choose *materials*))

(defun material-price (material) 
  (declare (ignore material))
  100)

(defparameter *lock-on-time* 420)

(defclass ship (cursor)
  ((image :initform "vomac.png")
   (target :initform nil)
   (lock-channel :initform nil :accessor lock-channel)
   (lock-begin :initform 0)
   (radar-range :initform 400)
   (cursor-class :initform 'ship)
   (shield-damaged-p :initform nil :accessor shield-damaged-p)
   (radar-damaged-p :initform nil :accessor radar-damaged-p)
   (targeting-damaged-p :initform nil :accessor targeting-damaged-p)
   (cargo :initform nil :accessor cargo :initarg cargo)
   (stagger :initform 0)
   (lock :initform 0)
   (locking :initform nil)
   (altitude :initform *maximum-altitude*)
   (fuel :initform *maximum-fuel*)
   (energy :initform *maximum-energy*)
   (shield :initform *maximum-shield*)
   (missiles :initform 2)
   (bullet-reload-timer :initform 0)
   (credits :initform 50 :accessor credits :initarg :credits)
   (max-speed :initform 3)
   (max-acceleration :initform 0.1)
   (tx :initform 0)
   (ty :initform 0)
   (dx :initform 0.0)
   (dy :initform 0.0)
   (ddx :initform 0.0)
   (ddy :initform 0.0)))

(defun hitzone-enemies ()
  (remove-if-not #'hitzone-p (find-enemies)))

(defmethod find-target ((ship ship))
  (let ((hitzoners (hitzone-enemies)))
    (if hitzoners
	(first (sort hitzoners #'< :key #'distance-to-ship))
	(nearest-enemy))))

(defmethod add-material ((ship ship) material units)
  (show-message (format nil "Retrieved ~A units of ~A" units material))
  (push (list material units) (cargo ship)))

(defmethod empty-cargo ((ship ship))
  (setf (cargo ship) nil))

(defmethod cargo-value ((ship ship))
  (let ((value 0))
    (dolist (entry (cargo ship))
      (destructuring-bind (material units) entry
	(incf value (* (material-price material) units))))
    value))

(defmethod sell-cargo ((ship ship))
  (let ((price (cargo-value ship)))
    (show-message (format nil "Sold cargo materials for ~A credits." price))
    (credit ship price)
    (empty-cargo ship)))
  
(defmethod current-balance ((ship ship))
  (credits ship))

(defmethod credit ((ship ship) amount)
  (incf (credits ship) amount)
  (setf (credits ship) (round (credits ship))))

(defmethod credits-available-p ((ship ship) amount)
  (<= amount (current-balance ship)))

(defmethod debit ((ship ship) amount)
  (decf (credits ship) amount)
  (setf (credits ship) (round (max 0 (credits ship)))))

(defparameter *credits-per-endurium* 274)

(defun endurium-price (units) 
  (round (* units *credits-per-endurium*)))

(defmethod burn-endurium ((ship ship) units)
  (with-slots (fuel) ship
    (setf fuel (max 0 (decf fuel units)))))

(defmethod restore-endurium ((ship ship) units)
  (with-slots (fuel) ship
    (setf fuel (max 0 (min *maximum-fuel* (incf fuel units))))))

(defmethod remove-missile ((ship ship) &optional (units 1))
  (with-slots (missiles) ship
    (setf missiles (max 0 (decf missiles units)))))

(defmethod add-missile ((ship ship) &optional (units 1))
  (with-slots (missiles) ship
    (setf missiles (max 0 (min *maximum-missiles* (incf missiles units))))))

(defmethod buy-fuel ((ship ship) units)
  (let ((price (endurium-price units)))
    (when (credits-available-p ship price)
      (prog1 price
	(debit ship price)
	(restore-endurium ship units)))))

(defparameter *credits-per-missile* 180)

(defmethod buy-missile ((ship ship) units)
  (let ((price *credits-per-missile*))
    (when (credits-available-p ship price)
      (prog1 price
	(debit ship price)
	(add-missile ship units)))))

(defmethod lock-on ((ship ship))
  (with-slots (target lock-channel lock locking lock-begin) ship
    (setf target nil)
    (setf lock-begin *updates*)
    (setf lock *lock-on-time*)
    (setf locking t)
    (roger :targeting t)
    (setf lock-channel (play-sample "lockon.wav" :loop t))))

(defmethod cancel-lock-on ((ship ship))
  (with-slots (lock-channel lock locking lock-begin target) ship
    (when lock-channel (halt-sample lock-channel))
    (setf target nil)
    (play-sample "powerdown.wav")
    (setf lock-begin 0 lock 0 locking nil lock-channel nil)))

(defmethod lock-length ((ship ship))
  (- *updates* (slot-value ship 'lock-begin)))

(defparameter *firing-timer-threshold* 180)
(defun lock-timeout () (seconds->frames 8))

(defun lock-commit-p ()
  (>= *firing-timer-threshold* (slot-value (ship) 'lock)))

(defmethod lock-delta ((ship ship)) 
  (cond 
    ((< (slot-value ship 'lock) *firing-timer-threshold*) 1)
    ((hitzone-enemies) 1)
    ((lock-on-p ship) 0)
    (t 1)))

(defmethod update-lock ((ship ship))
  (when (lock-on-p ship)
    (if (and (> (lock-length ship) (lock-timeout))
	     (not (lock-commit-p)))
	(cancel-lock-on ship)
	(with-slots (target lock lock-channel locking) ship
	  (decf lock (lock-delta ship))
	  ;; announce firing
	  (when (= *firing-timer-threshold* lock)
	    (setf target (find-target ship))
	    (play-sample "radar-lock.wav")
	    (roger :firing t))))))

(defmethod lock-on-p ((ship ship))
  (plusp (slot-value ship 'lock)))

(defmethod lock-on-complete-p ((ship ship))
  (= 1 (slot-value ship 'lock)))

(defmethod boosting-p ((ship ship)) nil)

(defmethod energy-rate ((ship ship))
  (if (boosting-p ship) 0.1 0.01))

(defmethod update-energy ((ship ship))
  (with-slots (energy) ship
    (when (null energy)
      (setf energy *maximum-energy*))
    (when (plusp energy)
      (decf energy (energy-rate ship)))))

(defmethod die ((ship ship))
  (play-explosion-sound)
  (play-explosion-sound)
  (stop-engine-sound)
  (stop-thrust-sound)
  (visit (game-over-screen)))

(defmethod damage-shield ((ship ship) points)
  (play-damage-sound)
  (when (shield-damaged-p ship)
    (setf points (* 2 points)))
  (with-slots (shield) ship
    (decf shield points)
    (when (minusp shield)
      (die ship))))
  
(defmethod damage-shield-system ((ship ship))
  (roger :shield-damaged)
  (setf (shield-damaged-p ship) t))

(defmethod damage-radar-system ((ship ship))
  (roger :radar-damaged)
  (setf (radar-damaged-p ship) t))

(defmethod damage-targeting-system ((ship ship))
  (roger :targeting-damaged)
  (setf (targeting-damaged-p ship) t))

(defmethod repair-shield-system ((ship ship))
  (roger :shield-repaired)
  (setf (shield-damaged-p ship) nil))

(defmethod repair-radar-system ((ship ship))
  (roger :radar-repaired)
  (setf (radar-damaged-p ship) nil))

(defmethod repair-targeting-system ((ship ship))
  (roger :targeting-repaired)
  (setf (targeting-damaged-p ship) nil))

(defmethod damage-random-system ((ship ship))
  (case (random 3)
    (0 (when (not (targeting-damaged-p ship))
	 (damage-targeting-system ship)))
    (1 (when (not (shield-damaged-p ship))
	 (damage-shield-system ship)))
    (2 (when (not (radar-damaged-p ship))
	 (damage-radar-system ship)))))

(defmethod repairs-needed-p ((ship ship))
  (or (targeting-damaged-p ship)
      (shield-damaged-p ship)
      (radar-damaged-p ship)))

(defmethod damage-shield :after ((ship ship) points)
  (setf (slot-value ship 'stagger) 80)
  (cond ((> points 800)
	 (percent-of-time 50 (roger :shields-taking-heavy-damage)))
	((> points 300)
	 (percent-of-time 20 (roger :shields-under-stress))))
  (when (> points 800)
    (percent-of-time 21 (damage-random-system ship))))

(defmethod restore-shield ((ship ship) points)
  (with-slots (shield) ship
    (setf shield (min *maximum-shield* (+ shield points)))))

(defmethod restore-fuel ((ship ship) points)
  (with-slots (fuel) ship
    (setf fuel (min *maximum-fuel* (+ fuel points)))))

(defmethod use-energy ((ship ship) amount)
  (decf (field-value 'energy ship) amount))

(defmethod fire ((ship ship) (bullet bullet))
  (with-slots (bullet-reload-timer) ship
    (setf bullet-reload-timer *bullet-reload-time*))
  (play-fire-sound)
  (multiple-value-bind (x y) (firing-location)
    (add-node (current-buffer) (make-instance 'bullet) x y)))

(defparameter *missile-cost* 12)

(defparameter *warned* nil)

(defmethod fire ((ship ship) (missile missile))
  (multiple-value-bind (x y) (firing-location)
    (add-node (current-buffer) (make-instance 'missile) x y))
  (remove-missile ship))

(defmethod update :before ((ship ship))
  (update-energy ship)
  (with-slots (bullet-reload-timer stagger) ship
    (when (plusp bullet-reload-timer)
      (decf bullet-reload-timer))
    (when (plusp stagger)
      (decf stagger))))

(defmethod explode-when-empty ((ship ship))
  (when (and (not *warned*) 
	     (< (fuel) 13))
    (setf *warned* t)
    (play-sample "alarm.wav")
    (roger :endurium-low))
  (when (not (plusp (fuel)))
    (die ship)))

(defmethod update :after ((ship ship))
  (explode-when-empty ship))

(defmethod ready-to-fire-p ((ship ship))
  (zerop (field-value 'bullet-reload-timer ship)))

(defmethod last-target ((ship ship))
  (with-slots (target) ship
    (if (xelfp target)
	target
	(prog1 (nearest-enemy)
	  (setf target nil)))))

(defmethod run ((ship ship)) 
  ;; bullets
  (when (and (holding-fire) (ready-to-fire-p ship))
    (fire ship (make-instance 'bullet)))
  ;; missile lock-on
  (when (and (holding-alt-fire) 
	     (not (targeting-damaged-p ship))
	     (not (slot-value ship 'locking))
	     (find-enemies))
    (when (plusp (slot-value ship 'missiles))
      (lock-on ship))) 
  (update-lock ship)
  (when (lock-on-complete-p ship)
    (cancel-lock-on ship) 
    (fire ship (make-instance 'missile :target (last-target ship)))))

(defparameter *climb-amplitude* 120)

(defmethod climb-delta ((ship ship))
  (with-slots (dy) ship
    (* -1 *climb-amplitude* (/ dy (max-speed ship)))))

(defmethod stick-heading ((ship ship))
  (or (when (left-analog-stick-pressed-p *player-1-joystick*)
	(left-analog-stick-heading *player-1-joystick*))
      (when (right-analog-stick-pressed-p *player-1-joystick*)
	(right-analog-stick-heading *player-1-joystick*))))

(defvar *inverted-control* t)

(defun controlling-p ()
  (or (arrow-direction)
      (left-analog-stick-pressed-p *player-1-joystick*)))

(defmethod control-heading ((self ship))
  (when (controlling-p)
    (let ((dir (or (arrow-direction)
		   (when (stick-heading self)
		     (or (heading-direction (stick-heading self))
			 :left)))))
      (when dir (direction-heading dir)))))

(defmethod control-pressure ((self ship))
  (when (controlling-p) 0.2))

(defparameter *default-thrust* 0.2)

(defparameter *analog-thrust-scale* 0.1) ;; was 0.06

(defmethod thrust-x ((self ship)) 
  (when (controlling-p)
    (* *analog-thrust-scale* 
       (cos (control-heading self))
       (control-pressure self))))

(defmethod thrust-y ((self ship)) 
  (when (controlling-p)
    (* *analog-thrust-scale*
       (* (sin (control-heading self))
	  (if *inverted-control* -1 1))
       (control-pressure self))))
  
(defmethod max-thrust ((self ship)) 
  (max-speed self))

(defmethod booster-thrust ((self ship)) 0.1)
  ;; (if (holding-fire) 2 0.1))

(defmethod thrust-z ((self ship))
  (booster-thrust self))

(defmethod update-thrust :before ((self ship))
  (with-slots (tx ty tz) self
    (let ((thrusting (or tx ty)))
      (when (and thrusting (not (thrust-sound-p)))
	(play-thrust-sound))
      (when (not thrusting)
	(stop-thrust-sound)))))

(defmethod update-thrust ((self ship))
  (with-slots (tx ty tz) self
    (setf tx (thrust-x self))
    (setf ty (thrust-y self))
    (setf tz (thrust-z self))))

(defconstant +normal+ (- (/ pi 2)))

(defparameter *ship-heading-drag* 8)

(defun ship-flightpath-left-p ()
  (multiple-value-bind (top left right bottom) (bounding-box (ship))
    (> 5 (abs (- (flightpath-left) left)))))

(defun ship-flightpath-right-p ()
  (multiple-value-bind (top left right bottom) (bounding-box (ship))
    (> 5 (abs (- (flightpath-right) right)))))
     
(defmethod update-heading ((ship ship))
  (with-slots (heading dx) ship
    (let* ((max-speed (max-speed ship))
	   (rate (/ dx *ship-heading-drag*)))
	  (setf heading 
		(if (plusp dx)
		    (+ +normal+ (* *max-roll* rate))
		    (- +normal+ (* (- *max-roll*) rate)))))))
	  
(defmethod initialize-instance :after ((ship ship) &key)
  (setf *warned* nil)
  (setf *ship* ship))

(defmethod draw ((ship ship)) nil)

(defmethod render ((ship ship))
  (with-slots (x y width height heading image) ship
    ;; fake perspective on ship
    (draw-textured-rectangle-* (- x (units 2)) 
			       (- y (/ (horizon-delta) 40))
			       0 
			       (image-width image) 
			       (- (image-height image) (/ (horizon-delta) 50))
			       (find-texture image)
			       :angle 
			       (radian-angle (roll)))))

;;; Debris

(defparameter *debris-images* (image-set "debris" 4))

(defclass debris (texel)
  ((image-scale :initform (random-choose '(1.0 1.5 2.2)))
   (image :initform (random-choose *debris-images*))
   (timer :initform (random-choose '(100 130 160)))
   (x0 :initform 0)
   (y0 :initform 0)))

(defmethod initialize-instance :after ((debris debris) &key x y)
  (with-slots (x0 y0) debris
    (setf x0 x y0 y)))

(defparameter *debris-speed* 2) 

(defmethod run ((debris debris))
  (with-slots (x y x0 y0 timer) debris
    (when (plusp timer)
      (decf timer)
      (move debris (find-heading x0 y0 x y) *debris-speed*))
    (when (zerop timer)
      (destroy debris))))

;;; Basic enemy weapon 

(defparameter *proton-decay-time* 200)
(defparameter *proton-speed* 3.4)
(defparameter *proton-size* 8)
(defparameter *proton-scaling-factor* 1.007)
(defparameter *proton-images* (image-set "proton" 5))

(defclass proton (texel)
  ((image :initform "proton-1.png")
   (timer :initform *proton-decay-time*)))

(defmethod initialize-instance :after ((proton proton) &key heading)
  (setf (field-value 'heading proton) heading)
  (resize proton *proton-size* *proton-size*))

(defmethod run ((proton proton))
  (with-slots (height width image heading timer) proton
    (move proton heading *proton-speed*)
    (setf image (random-choose *proton-images*))
    (resize proton (* width *proton-scaling-factor*) 
	    (* height *proton-scaling-factor*))
    (decf timer)
    (when (minusp timer)
      (destroy proton))))

(defmethod collide ((proton proton) (ship ship))
  (multiple-value-bind (x y) (center-point proton)
    (make-sparks x y (random-choose '(10 15 20))))
  (damage-shield ship (random-choose '(380 480 550)))
  (destroy proton))

;;; Enemies
  
(defclass enemy (texel)
  ((alerted :initform nil)
   (armor :initform 20)
   (image :initform "scanner.png")
   (heading :initform (random (* 2 pi)))))

(defun find-enemies ()
  (let (enemies)
    (maphash #'(lambda (k v)
		 (when (typep (find-object k) (find-class 'enemy))
		   (push (find-object k) enemies)))
	     (slot-value (current-buffer) 'objects))
    enemies))

(defparameter *enemy-near-distance* 700)

(defmethod near-ship ((self texel))
  (< (distance-between self (ship))
     *enemy-near-distance*))

(defun nearby-enemies-p ()
  (some #'near-ship (find-enemies)))

(defun nearest-enemy ()
  (let ((enemies (find-enemies)))
    (setf enemies (sort enemies #'< :key #'(lambda (x) (distance-between x (ship)))))
    (first enemies)))
      
(defmacro define-enemy (name &body body)
  `(defclass ,name (enemy)
     ,@body))

(defmethod damage-armor ((enemy enemy) &optional (points 1))
  (with-slots (armor) enemy
    (decf armor points)
    (play-whack-sound)
    (unless (plusp armor)
      (explode enemy))))

(defmethod attack ((enemy enemy) &optional heading)
  (multiple-value-bind (x y) (center-point enemy)
    (when *use-sound* (play-sample "bump-1.wav"))
    (add-node (current-sector) (make-instance 'proton :heading (or heading (heading-to-ship enemy))) x y)))

(defmethod explode ((enemy enemy))
  (multiple-value-bind (x y) (center-point enemy)
    (dotimes (n 5)
      (add-node (current-sector)
		   (make-instance 'debris :x (+ -5 x (random 10)) :y (+ -5 y (random 10))) x y))
    (make-sparks x y (random-choose '(10 15 20)))
    (play-explosion-sound)
    (percent-of-time 35
      (add-node (current-sector) (make-instance 'endurium-chunk) x y)
      (play-sample (random-zap-sound))
      (play-sample (random-zap-sound)))
    (destroy enemy)))

(defmethod think ((self enemy))
  (with-slots (x y alerted) self
    (when (and (not alerted)
	       (< (distance-to-ship self) 700))
      (setf alerted t)
      (play-doppler-sound))
    (if (hitzone-p self)
	(percent-of-time 2
	  (play-doppler-sound)
	  (if (percent-of-time 40 t)
	      (slide-to self (random-choose (list (dodge-left) (dodge-right))) y)
	      (slide-to self (random-choose (list (escape-left) (escape-right))) (escape-bottom))))
	(let ((x0 (+ x (random-choose '(-200 -100 50 100 200))))
	      (y0 (+ y (- (random 200) 100))))
	  (when (not (sliding-p self))
	    (slide-to self x0 y0 :interpolation :sine))))))
  
(defmethod collide ((enemy enemy) (bullet bullet))
  (multiple-value-bind (x y) (center-point bullet)
    (make-sparks x y (random-choose '(10 13 17))))
  (damage-armor enemy 5)
  (destroy bullet))

(defmethod collide ((enemy enemy) (missile missile))
  (multiple-value-bind (x y) (center-point enemy)
    (make-sparks x y (random-choose '(10 13 17))))
  (damage-armor enemy 30)
  (destroy missile))

(defmethod collide ((bullet bullet) (enemy enemy))
  (multiple-value-bind (x y) (center-point bullet)
    (make-sparks x y (random-choose '(10 13 17))))
  (damage-armor enemy 5)
  (destroy bullet))

(defmethod collide ((missile missile) (enemy enemy))
  (multiple-value-bind (x y) (center-point enemy)
    (make-sparks x y (random-choose '(10 13 17))))
  (damage-armor enemy 30)
  (destroy missile))

(defmethod run ((self enemy))
  (decf (field-value 'slide-timer self))
  (with-slots (x y) self
    (when (> (distance-to-ship self) 1000)
      (percent-of-time 5
  	(multiple-value-bind (x0 y0) (step-coordinates x y (heading-to-ship self) -611) ;; FIXME
  	  (slide-to self x0 y0)))))
  (when (and (not (sliding-p self))
  	     (< (distance-to-ship self) 2000))
    (think self))
  (when (and (sliding-p self)
	     (< (distance-to-ship self) 600))
    (percent-of-time 0.2 (attack self))))

(define-enemy scanner
  ((image-scale :initform 1.32)))

(defmethod think :after ((scanner scanner))
  (percent-of-time 1 (attack scanner)))

(define-enemy hyperceptor
  ((image :initform "hyperceptor.png")
   (image-scale :initform 2.1)
   (armor :initform 43)))

(defmethod update :after ((hyperceptor hyperceptor))
  (percent-of-time 0.2 (attack hyperceptor))
  (percent-of-time 0.1
    (when (lock-on-p (ship))
      (multiple-value-bind (x y) (center-point hyperceptor)
	(make-ions x y 10))
      (play-sample "interference.wav")
      (roger :targeting-error)
      (cancel-lock-on (ship)))))

(define-enemy corruptor
  ((image :initform "corruptor.png")
   (image-scale :initform 1.5)
   (armor :initform 1)))

(defmethod run ((corruptor corruptor))
  (when (< (distance-to-ship corruptor) 700)
    (move corruptor (heading-to-ship corruptor) 2)
    (percent-of-time 4 (when *use-sound* (play-sample "creep.wav")))))
 
(defmethod collide ((corruptor corruptor) (ship ship))
  (damage-shield ship 1100)
  (explode corruptor))

(define-enemy bomber 
  ((image :initform "rook.png")
   (armor :initform 60)
   (image-scale :initform 1.6)))

(defmethod run :after ((bomber bomber))
  (when (< (distance-to-ship bomber) 700)
    (percent-of-time 0.29 (drop bomber (make-instance 'corruptor) 20 20 0))))

(define-enemy drone 
  ((image :initform "drone.png")
   (image-scale :initform 1.7)))

(defmethod think :after ((drone drone))
  (move drone (heading-to-ship drone) 3)
  (percent-of-time 0.2 (attack drone)))
    
;;; Space combat starfield arena

(defclass flight (sector)
  ((depth :initform nil)
   (particles :initform nil)
   (background :initform nil)
   (sky :initform nil)
   (terrain :initform nil)
   (skyline :initform nil)
   (description :initform nil :initarg :description :accessor description)
   (scanner-display-p :initform nil)
   (help-string :initform "[Spacebar/JMenu] Open Menu   [Tab/Return/Enter/JAltFire] Fire missile    [Escape] Exit sector")
   (nodes :initform (list (button 'disengage-flight "disengage flight mode")))))

(defmethod find-description ((flight flight))
  (slot-value flight 'description))

(defmethod show-description ((flight flight))
  (show-message (find-description flight)))

(defun stop-lock-sound ()
  (with-slots (lock-channel) (ship)
    (halt-sample lock-channel)))

(defmethod exit :before ((flight flight))
  (stop-thrust-sound)
  (stop-engine-sound)
  (stop-lock-sound)
  (roger-reset))

(defmethod engage ((flight flight)) nil) ;; for now

(defparameter *camera-upright* 0)	
	    
(defparameter *skybox-scale-factor* 1.7)

(defmethod background-bounding-box ((flight flight))
  (let ((box (multiple-value-list (window-bounding-box flight))))
    (multiple-value-bind (top left right bottom)
  	(scale-bounding-box box *skybox-scale-factor*)
      (values top left right bottom))))

(defmethod sky-bounding-box ((flight flight))
  (let ((hy (horizon-y)))
    (multiple-value-bind (top left right bottom) (background-bounding-box flight) 
      (values (+ top (horizon-delta)) left right hy))))

(defmethod terrain-bounding-box ((flight flight))
  (multiple-value-bind (top left right bottom) (background-bounding-box flight) 
    (values (horizon-y) left right (+ bottom (horizon-delta)))))

(defmethod paint-background ((flight flight) color &optional (alpha 1.0))
  (multiple-value-bind (top left right bottom) (background-bounding-box flight)
    (draw-box left top (- right left) (- bottom top)
	      :color color
	      :alpha alpha)))

(defmethod texture-background ((flight flight) image &optional (alpha 1.0))
  (multiple-value-bind (top left right bottom) (background-bounding-box flight)
    (draw-image image left top :opacity alpha :width (- right left) :height (- bottom top))))

(defmethod paint-sky ((flight flight) color &optional (alpha 1.0))
  (multiple-value-bind (top left right bottom) (sky-bounding-box flight)
    (draw-box left top (- right left) (- bottom top)
	      :color color
	      :alpha alpha)))

(defmethod texture-sky ((flight flight) image &optional (alpha 1.0))
  (multiple-value-bind (top left right bottom) (sky-bounding-box flight)
    (draw-image image left top :opacity alpha :width (- right left) :height (- bottom top))))

(defmethod paint-terrain ((flight flight) color &optional (alpha 1.0))
  (multiple-value-bind (top left right bottom) (terrain-bounding-box flight)
    (draw-box left top (- right left) (- bottom top)
	      :color color
	      :alpha alpha)))

(defmethod texture-terrain ((flight flight) image &optional (alpha 1.0))
  (multiple-value-bind (top left right bottom) (terrain-bounding-box flight)
    (draw-image image left top :opacity alpha :width (- right left) :height (- bottom top))))

(defmethod draw-background ((flight flight))
  (with-slots (background) flight
    (cond ((texelp background) 
	   (draw background))
	  ((color-name-p background)
	   (paint-background flight background))
	  ((image-name-p background)
	   (texture-background flight background)))))

(defmethod draw-sky ((flight flight))
  (with-slots (sky) flight
    (cond ((texelp sky) 
	   (draw sky))
	  ((color-name-p sky)
	   (paint-sky flight sky))
	  ((image-name-p sky)
	   (texture-sky flight sky)))))

(defmethod draw-terrain ((flight flight))
  (with-slots (terrain) flight
    (cond ((texelp terrain) 
	   (draw terrain))
	  ((color-name-p terrain)
	   (paint-terrain flight terrain))
	  ((image-name-p terrain)
	   (texture-terrain flight terrain)))))

(defmethod draw-skyline ((flight flight)) nil)

(defun format-energy-string (&optional (energy (energy)))
  (format nil "ENERGY: ~2,'0D" (truncate energy)))

(defun format-missiles-string (&optional (missiles (slot-value (ship) 'missiles)))
  (format nil "MISSILES: ~D" (truncate missiles)))

(defun format-shield-string (&optional (shield (shield)))
  (format nil "SHIELD: ~1,'0D" (truncate shield)))

(defun format-fuel-string (&optional (fuel (fuel)))
  (format nil "ENDURIUM: ~,01f" fuel))

(defun format-credits-string (&optional (credits (credits (ship))))
  (format nil "CREDITS: ~S" (truncate credits)))

(defparameter *hud-font* "sans-mono-bold-10")

(defmethod draw-gun-reticle ((flight flight))
  (when (nearest-enemy)
    (let ((radius (units 2.2)))
      (multiple-value-bind (x y) (center-point (nearest-enemy))
	(draw-textured-rectangle-* (- x radius) (- y radius) 0 (* 2 radius) (* 2 radius)
				   (find-texture "round-reticle.png")
				   :opacity 0.1
				   :blend :additive
				   :vertex-color (if (hitzone-enemies)
						     (random-choose '("yellow" "green"))
						     (random-choose '("cyan" "magenta"))))))))

(defparameter *enemy-radar-color* "magenta")

(defmethod draw-radar ((flight flight))
  (multiple-value-bind (top left right bottom)
      (values (radar-top) (radar-left) (radar-right) (radar-bottom))
    (let ((width (- right left))
	  (height (- bottom top))
	  (sx (ship-x))
	  (sy (ship-y)))
      (draw-box left top width height :color "black" :alpha 0.1)
      (draw-textured-rectangle-* left top 0 width height (find-texture "reticle.png") :opacity 0.1)
      (maphash #'(lambda (k v)
		   (with-slots (x y) (find-object k)
		     (when (typep (find-object k) (find-class 'enemy))
		       (draw-box (+ left (/ width 2) (* 2/3 (/ (- x sx) (/ *flight-size* width))))
				 (+ top  (/ height 2) (* 2/3 (/ (- y sy) (/ *flight-size* height))))
				 3 3 :color *enemy-radar-color*))))
	       (field-value 'objects (current-sector))))))

(defmethod render-ship ((flight flight))
  (when (ship)
    (render (ship))))

(defmethod draw-particles ((flight flight))
  (let ((vec (make-array 1 :adjustable t :fill-pointer 0)))
    (with-slots (particles) flight
      (gl:with-pushed-matrix 
	(gl:load-identity)
	(multiple-value-bind (top left right bottom) (window-bounding-box flight)
	  (multiple-value-bind (vx vy) (window-vanishing-point)
	    (let* ((cx (/ (+ right left) 2))
		   (cy (/ (+ bottom top) 2))
		   (gx (+ vx))
		   (gy (+ vy (horizon-delta))))
	      (gl:translate gx gy 0)
	      (gl:rotate (+ 90 (heading-degrees (roll))) 0 0 -1)
	      (gl:translate (- gx) (- 0 gy (- (horizon-delta))) 0)
	      (loop for particle being the hash-keys of particles do
		(vector-push-extend particle vec))
	      (setf vec (sort vec #'> :key #'(lambda (p)
					       (or (slot-value p 'z) 0))))
	      (loop for particle across vec do (render particle)))))))))
	    ;; (loop for particle being the hash-keys of particles do
	    ;;   (render particle))))))))

(defvar *vanishing-point-function* #'horizon-point)

(defmacro with-vanishing-point (function &body body)
  `(let ((*vanishing-point-function* ,function)) ,@body))

(defmacro with-scene-rotation (angle &body body) 
  (let ((x (gensym))
	(y (gensym))
	(top (gensym))
	(left (gensym))
	(right (gensym))
	(bottom (gensym)))
    `(gl:with-pushed-matrix
       (gl:load-identity) 
       (multiple-value-bind (,top ,left ,right ,bottom)
	   (window-bounding-box (current-sector))
	 (multiple-value-bind (,x ,y) (funcall *vanishing-point-function*)
	   (gl:translate (/ (- ,right ,left) 2) (/ (- ,bottom ,top) 2) 0)
	   (gl:rotate ,angle 0 0 -1)
	   (gl:translate (- ,x) (- ,y) 0)
	   ,@body)))))

(defun standard-rotation ()
  (+ 90 (heading-degrees (roll))))

(defmethod draw-lock-zone ((flight flight))
  (when (lock-on-p (ship))
    (with-slots (lock) (ship)
      (multiple-value-bind (top left right bottom) (hitzone)
	(draw-textured-rectangle-* left top 0 (- right left) (- bottom top)
				   (find-texture "round-reticle-2.png")
				   :blend :additive
				   :vertex-color 
				   (if (lock-commit-p)
				       (random-choose '("yellow" "magenta"))
				       (if (hitzone-enemies) 
					   "gold"
					   "gray30")))
	(let ((m (- 120 (/ (- *lock-on-time* lock) 4))))
	  (let ((cx (/ (+ left right) 2))
		(cy (/ (+ top bottom) 2)))
	    (draw-textured-rectangle-* (- cx m) (- cy m) 0
				       (* 2 m) (* 2 m)
				       (find-texture "round-reticle-3.png")
				       :angle (/ m 2)
				       :blend :additive
				       :vertex-color 
				       (if (lock-commit-p)
					   (random-choose '("yellow" "magenta"))
					   (if (hitzone-enemies) 
					       "yellow"
					       "gray20")))))))))

(defmethod draw-hud ((flight flight)) 
  (unless (radar-damaged-p (ship))
    (draw-radar flight))
  (set-blending-mode :alpha)
  (draw-lock-zone flight)
  (set-blending-mode :alpha)
  (draw-modeline flight))

(defmethod draw-texels ((flight flight))
  (with-slots (objects) flight
    (loop for object being the hash-keys of objects do
      (draw (find-object object)))))

(defmethod draw-debug ((flight flight))
  (multiple-value-bind (vx vy) (vanishing-point)
    (draw-string "V" vx vy :color (random-choose '("white" "cyan"))))
  (draw-string "Hy" (window-x) (horizon-y) :color (random-choose '("yellow" "green")))
  (draw-box (window-x) (horizon-y) *width* 2 :color (random-choose '("DodgerBlue1" "DodgerBlue3")))
  (multiple-value-bind (x y) (step-toward-heading (ship) (roll) 300)
    (draw-string "R" x y :color (random-choose '("yellow" "magenta")))))
  
(defmethod draw ((flight flight))
  (with-slots (objects heading depth sky particles background skyline
		terrain window-x window-y window-z) flight
    (project-orthographically t)
    (transform-window :x window-x :y window-y :z window-z)
    (gl:matrix-mode :modelview)
    (with-vanishing-point #'horizon-point
      (with-scene-rotation (standard-rotation)
	(draw-terrain flight)
	(draw-sky flight)
	(draw-particles flight)))
    (with-vanishing-point #'horizon-point
      (with-scene-rotation (standard-rotation)
	(when *debug* (draw-debug flight))
	(draw-texels flight)  
	(when (lock-on-p (ship))
	  (draw-gun-reticle flight))
	(render-ship flight))
      (draw-hud flight))))

(defmethod draw :after ((flight flight)) nil)

(defmethod toggle-debug ((flight flight))
  (setf *debug* (if *debug* nil t)))

(defmethod can-visit-p ((flight flight)) t)

(defclass void (flight) ((radiation :initform 0)))

(defmethod can-visit-p ((void void)) nil)

(defclass wall (flight) ((radiation :initform 0)))

(defmethod can-visit-p ((wall wall)) nil)

(defmethod visit :after ((flight flight)) 
  (when *use-sound* (play-music "engine.ogg" :loop t)) 
  (set-cursor flight (ship))
  (insert (ship))
  (with-slots (height width) (ship)
    (move-to (ship) (- (/ *flight-size* 2)
		       (/ width 2))
	     (- (/ *flight-size* 2)
		(/ height 2))))
  (update flight))

(defmethod update-camera ((self flight))
  (with-slots (background-x background-y window-x window-y) (current-buffer)
    (let ((window-y (- (ship-y) *ship-camera-y*))
	  (window-x (if (< (ship-x) (flightpath-left))
			(- (window-x) (- (flightpath-left) (ship-x)))
			(if (> (ship-x) (flightpath-right))
			    (+ (window-x) (- (ship-x) (flightpath-right)))
			    (window-x)))))
      (move-window-to (current-buffer) window-x window-y))))

(defmethod wraparound ((self flight))
  (when (ship)
    (with-slots (background-x background-y objects window-x window-y height width) self
      (let* ((sx (ship-x))
	     (sy (ship-y))
	     (window-sx (- sx window-x))
	     (window-sy (- sy window-y))
	     (cx
	       (cond ((< sx (wraparound-left)) 1)
		     ((> sx (wraparound-right)) -1)))
	     (cy
	       (cond ((< sy (wraparound-top)) 1)
		     ((> sy (wraparound-bottom)) -1)))
	     (dx (when cx (cfloat (* cx 1/3 width))))
	     (dy (when cy (cfloat (* cy 1/3 height)))))
	(when (or cx cy)
	  ;; now transform/update objects
	  (loop for object0 being the hash-values of objects do
	    (let ((object (find-object object0)))
	      (multiple-value-bind (x y) (location object)
		(move-to object
			 (mod (+ x (or dx 0)) width)
			 (mod (+ y (or dy 0)) height))
		;; wrap around any objects needing transformed coordinates
		(wrap object (or dx 0) (or dy 0)))))
	  ;; realign camera
	  (move-window-to self
			  (- (ship-x) window-sx)
			  (- (ship-y) window-sy)))))))

(defmethod initialize-instance :after ((flight flight) &key (row 0) (column 0))
  (bind-event flight '(:escape) 'exit) 
  (bind-event flight '(:space) 'open-menu) 
  ;; (bind-event flight '(:d) 'toggle-debug)
  ;; (bind-event system '(:raw-joystick 0 :button-down) 'open-menu)
  ;; (bind-event flight '(:r :control) 'reset-game)
  ;; (bind-event flight '(:q :control) 'quit-game)
  (resize flight *flight-size* *flight-size*)
  (with-slots (particles quadtree-depth depth) flight
    (setf particles (make-hash-table :test 'eq))
    (setf quadtree-depth *skyw0r-quadtree-depth*)
    (setf depth *flight-size*)))

(defmethod open-menu :around ((flight flight))
  (if (find-enemies)
      (roger-error :no-exit)
      (call-next-method)))

;;; 3-D zooming particles

(defclass particle (texel)
  ((horizontal-follow-speed :initform nil))) 

(defmethod initialize-instance :after ((particle particle) &key)
  (with-slots (x y z) particle
    (setf x 0 y 0 z 0)))

(defmethod move-to ((particle particle) x0 y0 &optional z0)
  (with-slots (x y z) particle
    (setf x x0 y y0 z z0)))

(defmethod add-particle ((flight flight) (texel texel) x y z)
  (with-slots (particles) flight
    (setf (gethash texel particles) texel)
    (move-to texel x y z)))

(defmethod remove-particle ((flight flight) (texel texel))
  (with-slots (particles) flight
    (remhash texel particles)))

(defmethod destroy :before ((particle particle))
  (remove-particle (current-sector) particle))

(defparameter *horizontal-particle-follow-speed* 50)

(defmethod follow-ship ((particle particle))
  ;; follow ship horizontally to match perspective
  (with-slots (x horizontal-follow-speed) particle
    (decf x (* (or horizontal-follow-speed *horizontal-particle-follow-speed*)
	       (field-value 'dx (ship))))))

(defmethod wrap-x ((particle particle) offset)
  (with-slots (x) particle
    (incf x offset)))

(defmethod wrap-y ((particle particle) offset)
  (with-slots (y) particle
    (incf y offset)))

(defmethod wrap-z ((particle particle) offset)
  (with-slots (z) particle
    (incf z offset)))

(defmethod restrict-to-void ((particle particle))
  (with-slots (x y z) particle
    ;; wrap x
    (if (minusp x)
	(wrap-x particle *void-size*)
	(when (> x *void-size*)
	  (wrap-x particle (- *void-size*))))
    ;; wrap y
    (if (minusp y)
	(wrap-y particle *void-size*)
	(when (> y *void-size*)
	  (wrap-y particle (- *void-size*))))
    ;; wrap z
    (if (minusp z)
	(wrap-z particle *void-size*)
	(when (> z *void-size*)
	  (wrap-z particle (- *void-size*))))))

(defmethod update ((particle particle))
  (follow-ship particle) 
  (restrict-to-void particle)
  (run particle))
  
(defmethod update-particles ((flight flight))
  (with-slots (particles) flight
    (with-quadtree nil
      (loop for particle being the hash-keys of particles do
	(update particle)))))

(defmethod update-texels ((flight flight))
  (loop for object0 being the hash-values of (field-value 'objects flight) do
    (let ((object (find-object object0)))
      (update object))))

(defmethod sweep-around ((texel texel) amount)
  (when (> (distance-between texel (ship)) 1800)
    (multiple-value-bind (x y) (location texel)
      (let* ((x0 (- x (ship-x)))
	     (y0 (- y (ship-y)))
	     (theta (atan y0 x0))
	     (delta (* amount 0.001))
	     (d0 (distance x y (ship-x) (ship-y))))
	(move-to texel 
		 (+ (ship-x) (* d0 (cos (- theta delta))))
		 (+ (ship-y) (* d0 (sin (- theta delta)))))))))

(defmethod rotate-texels ((flight flight))
  (maphash #'(lambda (k v) (sweep-around (find-object k) (slot-value (ship) 'dx))) 
	   (slot-value flight 'objects)))

(defmethod update ((flight flight))
  (wraparound flight)
  (update-particles flight)
  (update-texels flight)
  (rotate-texels flight)
  (call-next-method)
  (when (ship)
    (update-camera flight)))

(defmethod glide-follow ((flight flight) (texel texel)) nil)

;;; Rushing dust particles

(defclass stardust (particle) 
  ((blend :initform :additive)
   (alpha :initform 0.5)
   (z :initform 0)
   (image :initform (random-choose '("star-1.png" "star-1.png" "star-1.png" "star-1.png" "star-4.png" "star-3.png" "star-2.png")))))

(defmethod initialize-instance :after ((stardust stardust) &key)
  (with-slots (height width) stardust
    (setf height 30 width 50)))

(defparameter *stardustfield-speed* 7)

(defmethod run ((stardust stardust))
  (with-slots (z) stardust
    (decf z (* *stardustfield-speed* (ship-speed)))))

(defmethod wrap-z :after ((stardust stardust) offset)
  (with-slots (x y) stardust
    (setf x (random *void-size*))
    (setf y (random *void-size*))))

;;; Luminous gas giant clouds

(defclass cloud (particle) 
  ((z :initform 0)
   (image-scale :initform 1.3)
   (blend :initform :additive)
   (alpha :initform 0.15)
   (image :initform (random-choose '("cloud-1.png" "cloud-2.png" "cloud-3.png" "cloud-4.png")))))

(defparameter *cloud-depth* 5)
(defparameter *cloud-offset* 500)

(defmethod run ((cloud cloud))
  (with-slots (z) cloud
    (decf z (/ (ship-speed) 1.0))))

(defmethod wrap-x :after ((cloud cloud) offset)
  (with-slots (y) cloud
    (setf y (+ (/ *void-size* 2) *cloud-offset* 
	       (cfloat (random (/ *void-size* *cloud-depth*)))))))

(defmethod initialize-instance :after ((cloud cloud) &key)
  (with-slots (width height) cloud
    (setf height (* 3 (random-choose '(80 120 160)))
	  width (* 3 (random-choose '(600 890 952 1200 1500 2200 2600))))))

;;; Purple and orange planets

(defclass planetar (particle)
  ((z :initform 0)
   (image-scale :initform 27)
   (blend :initform :additive)
   (horizontal-follow-speed :initform 30)
   (image :initform (random-choose '("planet-1.png" "planet-2.png" "planet-3.png")))))

(defmethod initialize-instance :after ((planetar planetar) &key image)
  (when image (setf (field-value 'image planetar) image)))

(defclass planetoid (planet)
  ((image-scale :initform 40)
   (horizontal-follow-speed :initform 30)
   (image :initform "planetoid.png")
   (blend :initform :additive)))

(defmethod restrict-to-void ((planetoid planetoid)) nil)

(defclass purple-planet (flight)
  ((sky :initform "sky-1.png")
   (terrain :initform "terrain-2.png")))

(defmethod visit :after ((purple-planet purple-planet))
  (roger :radiation))

(defmethod initialize-instance :after ((purple-planet purple-planet) &key)
  (add-particle purple-planet (make-instance 'planetar :image "planet-1.png") 
		(+ (/ *void-size* 2) 20000) (+ (/ *void-size* 2) -14000) (/ *void-size* 2.2))
  (dotimes (n 6)
    (add-node purple-planet (make-instance 'scanner) (random *flight-size*) (random *flight-size*)))
  (dotimes (n 70)
    (add-particle purple-planet (make-instance 'stardust) (random *void-size*) (random *void-size*) (random *void-size*)))
  (dotimes (n 20) 
    (add-particle purple-planet (make-instance 'cloud) (random *void-size*)
		  (+ (/ *void-size* 2) 500
		     (random (/ *void-size* *cloud-depth*)))
		  (random (* *void-size* 2)))))

(defclass orange-planet (flight)
  ((sky :initform "sky-2.png")
   (terrain :initform "terrain-1.png")))

(defmethod visit :after ((orange-planet orange-planet))
  (roger :gas-giant))

(defmethod initialize-instance :after ((orange-planet orange-planet) &key)
  (add-particle orange-planet (make-instance 'planetar :image "planet-2.png")
		(+ (/ *void-size* 2) 20000) (+ (/ *void-size* 2) -9000) (/ *void-size* 1.9))
  (dotimes (n 1)
    (add-node orange-planet (make-instance 'corruptor) (random *flight-size*) (random *flight-size*)))
  (dotimes (n 3)
    (add-node orange-planet (make-instance 'drone) (random *flight-size*) (random *flight-size*)))
  (dotimes (n 100)
    (add-particle orange-planet (make-instance 'stardust) (random *void-size*) (random *void-size*) (random *void-size*)))
  (dotimes (n 20) 
    (add-particle orange-planet (make-instance 'cloud) (random *void-size*)
		  (+ (/ *void-size* 2) 500
		     (random (/ *void-size* *cloud-depth*)))
		  (random (* *void-size* 2)))))

;;; Near cluster

(defclass globe (particle)
  ((image-scale :initform 10)
   (blend :initform :additive)
   (horizontal-follow-speed :initform 30)
   (image :initform (random-choose *star-cluster-images*))))

(defparameter *starfield-speed* 1)

(defmethod run ((globe globe))
  (with-slots (z) globe
    (decf z (* *starfield-speed* (ship-speed)))))

(defclass cluster (flight)
  ((background :initform "black")
   (sky :initform "sky-4.png")
   (terrain :initform "sky-6.png")))

(defmethod initialize-instance :after ((cluster cluster) &key)
  ;; (setf (slot-value cluster 'sky) "sky-1.png")
  ;; (setf (slot-value cluster 'terrain) "terrain-6.png")
  ;; (add-particle cluster (make-instance 'planetar :image "planet-3.png") 
  ;; 		(+ (/ *void-size* 2) 30000) (+ (/ *void-size* 2) -13000) (/ *void-size* 2.2))
  ;; (add-particle cluster (make-instance 'planetoid)
  ;; 		(+ (/ *void-size* 2) -20000) (+ (/ *void-size* 2) -2000) (/ *void-size* 1.9))
  (dotimes (n 2)
    (add-node cluster (make-instance 'bomber) (random *flight-size*) (random *flight-size*)))
  (dotimes (n 300)
    (add-particle cluster (make-instance 'stardust) (random *void-size*) (random *void-size*) (random *void-size*)))
  (dotimes (n 100)
    (add-particle cluster (make-instance 'globe) (random *void-size*) (random *void-size*) (random *void-size*))))

;; (defmethod visit :after ((cluster cluster))
;;   (roger :unrecognized))

;;; Asteroid field

(defclass endurium-crystal (particle)
  ((blend :initform :alpha)
   (horizontal-follow-speed :initform 30)
   (image :initform "proxbomb.png")))

(defmethod initialize-instance :after ((endurium-crystal endurium-crystal) &key)
  (resize endurium-crystal 300 300))

(defparameter *endurium-crystal-images* (image-set "crystal" 3))

(defmethod run ((endurium-crystal endurium-crystal))
  (with-slots (z image) endurium-crystal
    (setf image (random-choose *endurium-crystal-images*))
    (decf z (* *asteroid-field-speed* (ship-speed))))
  (collide-3d endurium-crystal (ship)))

(defun random-zap-sound ()
  (random-choose '("zap1.wav" "zap2.wav" "zap3.wav" "zap4.wav" "zap5.wav" "zap6.wav")))

(defmethod collide ((endurium-crystal endurium-crystal) (ship ship))
  (play-sample (random-zap-sound))
  (with-slots (x y) endurium-crystal
    (make-sparks (ship-x) (ship-y))
    (destroy endurium-crystal) 
    (restore-shield ship 40)
    (restore-fuel ship (random-choose '(1.3 0.7 0.8 0.5 1.2)))))

(defparameter *asteroid-images* (image-set "asteroid" 4)) 

(defclass asteroid-field (flight)
  ((sky :initform "sky-4.png")
   (terrain :initform "othersky.png")))

(defclass asteroid (particle) 
  ((blend :initform :alpha)
   (alpha :initform 0.5)
   (height :initform 150)
   (width :initform 150)
   (z :initform 0)
   (image :initform (random-choose *asteroid-images*))))

(defmethod initialize-instance :after ((asteroid asteroid) &key)
  (let ((size (random-choose '(700 900 1200))))
    (resize asteroid size size)))

(defparameter *asteroid-field-speed* 4)

(defmethod run ((asteroid asteroid))
  (with-slots (z) asteroid
    (decf z (* *asteroid-field-speed* (ship-speed)))
    (collide-3d asteroid (ship))))

(defmethod collide ((asteroid asteroid) (ship ship))
  (play-explosion-sound)
  (with-slots (x y) asteroid
    (make-sparks (ship-x) (ship-y) 10)
    (make-ions (ship-x) (ship-y) 10)
    (destroy asteroid) 
    (damage-shield ship 1200)))

(defmethod wrap-z :after ((asteroid asteroid) offset)
  (with-slots (x y) asteroid
    (setf x (random *void-size*))
    (setf y (random *void-size*))))

(defmethod initialize-instance :after ((asteroid-field asteroid-field) &key (asteroids 100) (crystals 24))
  (percent-of-time 50
    (dotimes (n 3)
      (add-node asteroid-field (make-instance 'yasichi) (random *flight-size*) (random *flight-size*))))
  (roger :endurium-detected)
  (dotimes (n 400)
    (add-particle asteroid-field (make-instance 'stardust) 
		  (random *void-size*) (random *void-size*) (random *void-size*)))
  (dotimes (n crystals)
    (add-particle asteroid-field (make-instance 'endurium-crystal)
		  (random *void-size*) (+ (/ *void-size* 2)) (random *void-size*)))
  (dotimes (n asteroids)
    (add-particle asteroid-field (make-instance 'asteroid)
		  (random *void-size*) (random *void-size*) (random *void-size*))))

(defclass anomalous-cloud (link)
  ((image :initform "rad.png")
   (description :initform "ASTEROID FIELD")
   (sector-class :initform 'asteroid-field)))

;;; Squadrons

(defclass squadron (flight)
  ((sky :initform "black")
   (terrain :initform "black")))

(defmethod initialize-instance :after ((squadron squadron) &key (enemies 2))
  (dotimes (n enemies)
    (add-node squadron (make-instance (random-choose '(scanner drone)))
	      (random *flight-size*) (random *flight-size*)))
  (dotimes (n 30)
    (add-particle squadron (make-instance 'globe) (random *void-size*) (random *void-size*) (random *void-size*)))
  (dotimes (n 500)
    (add-particle squadron (make-instance 'stardust) (random *void-size*) (random *void-size*) (random *void-size*))))

(defmethod visit :after ((squadron squadron))
  (when (find-enemies)
    (roger (if (= 1 (length (find-enemies)))
	       :contact-1
	       :contact-more))))

(defmethod exit :around ((squadron squadron))
  (if (find-enemies)
      (roger-error :no-exit)
      (call-next-method)))

(defmethod exit :before ((squadron squadron))
  ;; remove from galaxy map
  (destroy (link squadron)))

(defclass bomber-squadron (squadron) ())
(defmethod initialize-instance :after ((bomber-squadron bomber-squadron) &key)
  (add-node bomber-squadron (make-instance 'bomber) (random *flight-size*) (random *flight-size*)))

(defparameter *patrol-images* (image-set "patrol" 4))
(defparameter *patrol-sensor-range* 380)
(defparameter *patrol-engagement-range* 45)

(defclass patrol (link)
  ((sector-class :initform 'squadron)
   (description :initform "SQUADRON")
   (heading :initform (random (* 2 pi)))
   (image-scale :initform 0.35)))

(defmethod update ((patrol patrol))
  (let ((dist (distance-to-cursor patrol)))
    (with-slots (heading) patrol
      (cond ((< dist *patrol-engagement-range*)
	     (follow patrol (current-sector)))
	    ((< dist *patrol-sensor-range*)
	     (move patrol (heading-to-cursor patrol) 0.64))))))
	    ;;(t (move patrol heading 0.2)
;; (percent-of-time 0.01 (setf heading (random (* 2 pi)))))))))

(defmethod update :after ((patrol patrol))
  (restrict-to-buffer patrol))

(defmethod draw :around ((patrol patrol))
  (when (< (distance-to-ship patrol) (radar-range))
    (call-next-method)))

(defclass single-patrol (patrol)
  ((parameters :initform '(:enemies 1))
   (image :initform "patrol-1.png")))

(defclass double-patrol (patrol)
  ((parameters :initform '(:enemies 2))
   (image :initform "patrol-2.png")))

(defclass triple-patrol (patrol)
  ((parameters :initform '(:enemies 3))
   (image :initform "patrol-3.png")))

(defclass bomber-patrol (patrol)
  ((parameters :initform '(:enemies 3))
   (sector-class :initform 'bomber-squadron)
   (image :initform "patrol-4.png")))

;; ;;; Textured zooming heightmap terrain

(defclass clouds (texel)
  ((image :initform "terrain-strips.png")))

(defparameter *cloud-colors* '("goldenrod" "light goldenrod" "dark goldenrod" "coral" "violet red" "dark orange" "orange" "gold" "yellow" "magenta" "deep sky blue"))

(defparameter *cloud-brushes* (image-set "cloud-brush" 8))

(defparameter *cloud-pattern* nil)

(defparameter *pattern-length* 1000)

(defun make-cloud-pattern ()
  (let ((index 0)
	(pattern nil)
	(count (length *cloud-colors*)))
    (dotimes (n *pattern-length*)
      (push (list (nth index *cloud-colors*) 
		  (random-choose *cloud-brushes*))
	    pattern)
      (setf index (mod (+ index (random 3)) count)))
    pattern))

(setf *cloud-pattern* (make-cloud-pattern))

(defparameter *cloud-z* 0)

(defun find-cloud (z) 
  (nth (mod z *pattern-length*) *cloud-pattern*))

(defun cloud-color (z)
  (first (find-cloud z)))

(defun cloud-brush (z)
  (second (find-cloud z)))

(defmethod draw ((clouds clouds))
  (with-fields (image) clouds
    (multiple-value-bind (top left right bottom) (background-bounding-box (current-sector))
      (let* ((dy 3)
	     (z *cloud-z*)
	     (dz -0.01)
	     (y (horizon-y))
	     (texture-height (image-height image))
	     (texture-width (image-width image))
	     (last-y 0)
	     (height (/ (- bottom top) 2))
	     (end (+ y height))
	     (tw (- right left))
	     (th dy)
	     (strip 0.2))
	(loop while (< y end) do
	  (let* ((q (/ (focal-length) (+ 0.01 (abs (or z 0)))))
		 (ty (* (- (altitude) (/ *void-size* 2)) q)))
	    (draw-textured-rectangle-* left y 0 (- right left) 38
				       (find-texture (cloud-brush (truncate (/ z 100))))
				       :vertex-color (cloud-color (truncate (/ z 100))))
	    (incf z dz)
	    (setf *cloud-z* z)
	    (when (minusp z)
	      (incf z 1000))
	    (incf y dy)))))))

;;; Endurium crystals that aren't particles

(defclass endurium-chunk (texel)
  ((blend :initform :alpha)
   (image :initform "proxbomb.png")))

(defmethod initialize-instance :after ((endurium-chunk endurium-chunk) &key)
  (resize endurium-chunk 80 80))

(defparameter *endurium-chunk-images* (image-set "crystal" 3))

(defmethod run ((endurium-chunk endurium-chunk))
  (with-slots (image) endurium-chunk
    (setf image (random-choose *endurium-chunk-images*))
    (move endurium-chunk (opposite-heading (heading-to-ship endurium-chunk)) 5)))

(defmethod collide ((endurium-chunk endurium-chunk) (ship ship))
  (play-sample (random-zap-sound))
  (with-slots (x y) endurium-chunk
    (make-sparks (ship-x) (ship-y))
    (destroy endurium-chunk) 
    (restore-shield ship 1100)
    (restore-fuel ship (random-choose '(6 3.3 2.7 1.8 0.8 1.2)))))

;;; flight.lisp ends here

