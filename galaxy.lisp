(in-package :skyw0r)

(defvar *mission-x* 0)
(defvar *mission-y* 0)

(defun mission-coordinates () (values *mission-x* *mission-y*))

(defun random-encounter-coordinates (cx cy &optional (distance 1400))
  (multiple-value-bind (mx my) (step-coordinates cx cy (random (* 2 pi)) distance)
    (values mx my)))

(defun set-mission-coordinates (x y)
  (setf *mission-x* x *mission-y* y))

(defparameter *music-position* 0)
(defparameter *music-start-time* 0)

(defun begin-galaxy-music ()
  (play-music "xiomacs3.ogg" :loop nil)
  (setf *music-start-time* *updates*)
  (setf *music-position* 0))

(defun update-galaxy-music ()
  (let ((duration (- *updates* *music-start-time*)))
    (setf *music-position* (mod (/ duration (* 60))
				(minutes 4)))))

(defun resume-galaxy-music ()
  (play-music "xiomacs3.ogg" :loop nil)
  (seek-music (coerce *music-position* 'double-float)))

(defvar *sun-color* 10)
(defvar *sun-radius* 2)
(defvar *planet-color* nil)

;;; Generating a galaxy

(defparameter *main-sequence-star-classes* '(O B A F G K M))
(defparameter *maximum-habitable-temperature* 7000)
(defparameter *minimum-habitable-temperature* 4000)
(defparameter *other-star-classes* '(D C W X))

(defparameter *star-property-names* '(:tags :colors :frequency :minimum-radius :minimum-temperature :minimum-mass :names))

(defparameter *star-property-units* '(:mass solar-masses :temperature kelvin :radius solar-radii))

(defparameter *star-class-data* 
  '((O :names ("O-type main sequence star" "blue giant" "blue supergiant")
       :minimum-temperature 30000
       :minimum-mass 16
       :minimum-radius 6.6
       :colors ("white" "azure")
       :frequency 0.00003)
    (B :names ("B-type main sequence star" "blue giant" "blue supergiant")
       :minimum-temperature 10000
       :minimum-mass 2.1
       :minimum-radius 1.8
       :colors ("light cyan" "ghost white")
       :frequency 0.125) 
    (A :names ("A-type main sequence star")
       :minimum-temperature 7500
       :minimum-mass 1.4
       :minimum-radius 1.4
       :colors ("ghost white")
       :frequency 0.625)
    (F :names ("F-type main sequence star" "F-type giant" "F-type supergiant")
       :minimum-temperature 6000
       :minimum-mass 1.04
       :minimum-radius 1.15
       :colors ("ivory")
       :frequency 3.03)
    (G :names ("G-type main sequence star" "Yellow giant" "Yellow supergiant")
       :minimum-temperature 5200
       :minimum-mass 0.8
       :minimum-radius 0.96
       :colors ("light yellow" "lemon chiffon" "cornsilk")
       :frequency 7.5)
    (K :names ("K-type main sequence star" "K-type giant" "K-type supergiant" "K-type hypergiant")
       :minimum-temperature 3700
       :minimum-mass 0.45
       :minimum-radius 0.7
       :colors ("moccasin" "blanched almond" "bisque")
       :frequency 12.1)
    (M :names ("M-type main sequence star" "Red dwarf" "Red giant" "Red supergiant")
       :minimum-temperature 2400
       :minimum-mass 0.08
       :minimum-radius 0.4
       :colors ("orange red" "dark orange" "red")
       :frequency 76.45)))

(defun star-data (class) (rest (assoc class *star-class-data*)))
(defun star-property (class property) (getf (star-data class) property))
(defun star-names (class) (star-property class :names))
(defun star-minimum-temperature (class) (star-property class :minimum-temperature))
(defun star-minimum-mass (class) (star-property class :minimum-mass))
(defun star-minimum-radius (class) (star-property class :minimum-radius))
(defun star-colors (class) (star-property class :colors))
(defun star-frequency (class) (star-property class :frequency))

(defun random-star-class ()
  (block choosing
    (dolist (class *main-sequence-star-classes*)
      (let ((choice (percent-of-time (star-frequency class) class)))
	(when choice (return-from choosing choice))))
    'M))

(defun next-star-class (class)
  (if (eq class 'O) 'O
      (let ((pos (position class *star-class-data* :key #'first)))
	(when (plusp pos)
	  (first (nth (1- pos) *star-class-data*))))))

(defvar *cluster-heat* 0)

(defun locally-random-star-class ()
  (let ((class (random-star-class)))
    (when (plusp *cluster-heat*)
      (dotimes (n *cluster-heat*)
	(percent-of-time 60 (setf class (next-star-class class)))))
    (or class 'O)))

(defun star-maximum-temperature (class)
  (if (next-star-class class)
      (star-minimum-temperature (next-star-class class))
      100000))

(defun star-maximum-radius (class)
  (if (next-star-class class)
      (star-minimum-radius (next-star-class class))
      10000))

(defun star-maximum-mass (class)
  (if (next-star-class class)
      (star-minimum-mass (next-star-class class))
      100))

(defclass star (link)
  ((class-letter :initform 'M :accessor class-letter)
   (title :initform (random-system-name))
   (temperature :initform 0 :accessor temperature)
   (mass :initform 0 :accessor mass)
   (radius :initform 0 :accessor radius)
   (color :initform "orange red" :accessor color)
   (nodes :initform (list (button 'engage "visit star system")))))

(defmethod find-description ((star star)) 
  (slot-value star 'title))

(defmethod update ((star star)) nil)

(defmethod make-sector ((star star) parameters)    
  (with-slots (sector) star
    (setf *sun-color* (slot-value star 'color))
    (setf *sun-radius* (slot-value star 'radius))
    (setf sector (make-instance 'system))))

(defmethod initialize-instance :after ((star star) &key)
  (with-slots (class-letter temperature mass radius 
	       color image image-scale) star
    (setf class-letter (locally-random-star-class))
    (setf temperature (+ (star-minimum-temperature class-letter)
			 (random (+ 0.01 (- (star-maximum-temperature class-letter)
					    (star-minimum-temperature class-letter)))))
	  mass (+ (star-minimum-mass class-letter)
			 (random (+ 0.01 (- (star-maximum-mass class-letter)
					    (star-minimum-mass class-letter)))))
	  radius (+ (star-minimum-radius class-letter)
			 (random (+ 0.01 (- (star-maximum-radius class-letter)
					    (star-minimum-radius class-letter)))))
	  color (random-choose (star-colors class-letter))
	  image-scale (random-choose '(0.7 0.8 0.9))
	  image (random-choose *system-images*))
    (auto-resize star image-scale)))

(defmethod draw ((star star))
  (with-slots (x y width height image color) star
    (draw-textured-rectangle x y 0 width height (find-texture image) :blend :additive :vertex-color color)
    (draw-textured-rectangle x y 0 width height (find-texture image) :blend :additive :opacity 0.5)))

(defmethod draw-properties ((star star))
  (with-slots (class-letter temperature mass radius title color x y) star
    (let ((x0 (+ (window-x) 3))
	  (y0 (+ (window-y) 3))
	  (h (font-height *hud-font*))
	  (strings nil))
      (setf strings (list
		     (format nil "Title: ~S" title)
		     (first (star-names class-letter))
		     (format nil "Location: (~S, ~S)" x y)
		     (format nil "Temperature: ~S K" temperature)
		     (format nil "Mass: ~S solar masses" mass)
		     (format nil "Radius: ~S solar radii" radius)
		     (format nil "Color: x11 ~S" color)))
      (dolist (string strings)
	(draw-string string x0 y0 :color "white" :font *hud-font*)
	(incf y0 (1+ h))))))

;;; Suns

(defclass sun (texel)
  ((image :initform "planet-disc.png")
   (color :initform *sun-color*)))

(defmethod initialize-instance :after ((sun sun) &key color radius)
  (when color (setf (slot-value sun 'color) color))
  (when radius (resize sun (* radius 2) (* radius 2))))

;;; Planet links

(defparameter *planet-types* '(rock fungus desert ice ocean magma gas plasma crystal degenerate unknown))

(defclass planet (link)
  ((image :initform "planet-disc.png")
   (sector-class :initform (random-choose (mapcar #'planet-class-name *planet-types*)) :accessor sector-class)
   (image-scale :initform (random-choose '(0.2 0.3 0.4 0.5 0.7 0.9 1.1 2.1 4.1)))
   (color :initform "white")
   (nodes :initform (list (folder :planet 
				  (button 'engage "engage orbital flight mode")
				  (button 'file-report))))))

(defmethod find-description ((planet planet))
  (pretty-string (class-name (class-of (find-sector planet)))))

(defmethod draw ((planet planet))
  (with-slots (x y width height color) planet
    (draw-textured-rectangle 
     x y 0 width height 
     (find-texture "planet-disc.png") 
     :blend :alpha 
     :vertex-color color)))

(defun rock-terrain-colors ()
  (random-choose 
   '(("gray" "light gray" "dark gray" "sienna" "saddle brown" "dark khaki" "dark sea green")
     ("tan" "navajo white" "burlywood" "saddle brown" "gray40" "gray60")
     ("light sea green" "medium aquamarine" "pale goldenrod" "light goldenrod" "light sky blue")
     ("light steel blue" "cornflower blue" "wheat" "aquamarine" "pale green")
     ("powder blue" "pale turquoise" "light sky blue" "slate gray" "light slate gray")
     ("thistle" "plum" "dark slate blue" "misty rose" "peach puff" "gray50" "gray20"))))

(defun terrain-class-colors (type)
  (ecase (make-keyword type)
    (:fungus-planet '("yellow" "yellow green" "green yellow" "forest green" "chartreuse" "gold" "goldenrod"))
    (:rock-planet (rock-terrain-colors))
    (:desert-planet '("dark khaki" "khaki" "lemon chiffon"))
    (:ice-planet '("ivory" "honeydew" "mint cream" "light cyan" "lavender" "azure"))
    (:ocean-planet '("medium sea green" "dark turquoise" "turquoise" "pale turquoise" "powder blue" "sky blue" "cornflower blue"))
    (:magma-planet '("red" "orange red" "dark orange" "orange" "gold"))
    (:gas-planet '("indian red" "light coral" "salmon" "light salmon" "tomato" "coral"))
    (:plasma-planet '("purple" "dark orchid" "orchid" "light slate blue" "medium orchid" "violet"))
    (:crystal-planet '("white" "light cyan" "mint cream" "honeydew"))
    (:degenerate-planet '("dark gray" "slate gray" "light slate gray" "medium purple"))
    (:unknown-planet '("deep pink" "yellow green"))
    ;; the list repeats here
    (:fungus '("yellow" "yellow green" "green yellow" "forest green" "chartreuse" "gold" "goldenrod"))
    (:rock (rock-terrain-colors))
    (:desert '("dark khaki" "khaki" "lemon chiffon"))
    (:ice '("ivory" "honeydew" "mint cream" "light cyan" "lavender" "azure"))
    (:ocean '("medium sea green" "dark turquoise" "turquoise" "pale turquoise" "powder blue" "sky blue" "cornflower blue"))
    (:magma '("red" "orange red" "dark orange" "orange" "gold"))
    (:gas '("indian red" "light coral" "salmon" "light salmon" "tomato" "coral"))
    (:plasma '("purple" "dark orchid" "orchid" "light slate blue" "medium orchid" "violet"))
    (:crystal '("white" "light cyan" "mint cream" "honeydew"))
    (:degenerate '("dark gray" "slate gray" "light slate gray" "medium purple"))
    (:unknown '("deep pink" "yellow green"))))

(defmethod random-class-color ((planet planet))
  (random-choose (terrain-class-colors (make-keyword (slot-value planet 'sector-class)))))

(defmethod initialize-instance :after ((planet planet) &key color radius)
  (declare (ignore color))
  (setf (slot-value planet 'color)
	(random-choose (terrain-class-colors (sector-class planet))))
  (when radius (resize planet (* radius 2) (* radius 2))))

;;; Maps of the star system and its planets 

(defparameter *planet-choices*
  '((plasma plasma rock rock magma magma)
    (plasma rock rock rock desert desert magma)
    (rock rock rock fungus desert ocean gas)
    (rock desert fungus ocean ocean gas)
    (rock gas ice)
    (gas ice gas ice rock)
    (gas degenerate ice ice unknown rock)))

(defun random-planet-type-by-position (n)
  (planet-class-name (random-choose (nth n *planet-choices*))))
   
(defclass system (sector) ())

(defmethod find-ambient-nodes ((system system))
  (list (button 'double-exit "exit star system")))

(defmethod window-bounding-box ((system system))
  (values 0.0 0.0 *width* *height*))

(defmethod sun-position ((system system)) 
  (values 640 (/ 720 2)))

(defmethod entry-point ((system system))
  (values 200 (/ *height* 2)))

(defun radius-alpha () 20)

(defmethod initialize-instance :after ((system system) &key)
  (bind-event system '(:escape) 'exit) 
  (bind-event system '(:space) 'open-menu)
  (bind-event system '(:tab) 'use-scanner)
  (bind-event system '(:return) 'use-scanner)
  (bind-event system '(:raw-joystick 1 :button-down) 'use-scanner)
  (bind-event system '(:raw-joystick 0 :button-down) 'open-menu)
  (bind-event system '(:raw-joystick 999 :button-down) 'engage) 
  (resize system *width* *height*)
  (let ((cursor (make-instance 'cursor)))
    (add-node system cursor)
    (set-cursor system cursor)
    (move-cursor-to-entry-point system))
  (multiple-value-bind (cx cy) (sun-position system)
    (let ((sun (make-instance 'sun :color *sun-color* :radius (* (radius-alpha) *sun-radius*)))
	  (rx (+ (* *sun-radius* (radius-alpha)) 70 (random 10)))
	  (drx 60))
      (add-node system sun (- cx (* (radius-alpha) *sun-radius*)) (- cy (* (radius-alpha) *sun-radius*)))
      (dotimes (n (random (length *planet-choices*)))
	(let ((planet (make-instance 'planet :radius (+ 3 (random 18)))))
	  (multiple-value-bind (px py) (step-coordinates cx cy (cfloat (random (* pi 2))) (cfloat rx))
	    (add-node system planet px py)
	    (setf (slot-value planet 'sector-class)
		  (random-planet-type-by-position n))
	    (incf rx (+ drx (random 20)))))))))

(defmethod exit :before ((system system))
  (move-cursor-to-entry-point system))

(defmethod draw ((system system))
  (with-slots (objects window-x window-y window-z) system
    (project-orthographically t)
    (transform-window :x window-x :y window-y :z window-z)
    (xelf::draw-object-layer system)
    (when (typep (link system) (find-class 'star))
      (draw-properties (link system)))
    (multiple-value-bind (x y z width height) (cloud-coordinates system)
      (draw-textured-rectangle-* x y z width height (find-texture (default-system-background)) :blend :additive :opacity 0.01))
    (draw-modeline system)))

(defmethod visit :after ((system system))
  (move-window-to system 0 0))

(defun default-system-background () (progn "cloudy.png"))

(defmethod cloud-coordinates ((system system) &optional (background (default-system-background)))
  (if (null (link system))
      (values 0 0 0 3000 3000)
      (multiple-value-bind (x y) (center-point (link system))
	(let ((q 5))
	  (let ((tx (* q (/ (image-width background) x)))
		(ty (* q (/ (image-height background) y))))
	    (values (- 0 (* tx q)) (- 0 (* ty q)) 0 (* q (image-width background)) (* q (image-height background))))))))

(defmethod draw ((planet planet))
  (with-slots (x y width height color) planet
    (draw-textured-rectangle-* x y 0 width height (find-texture "planet-disc.png") :blend :alpha :vertex-color color))
  (multiple-value-bind (cx cy) (sun-position (current-sector))
    (multiple-value-bind (x y) (center-point planet)
      (let ((radius (distance cx cy x y)))
	(draw-textured-rectangle-* (- cx radius) (- cy radius) 0 (* radius 2) (* radius 2) (find-texture "orbit.png")
				   :vertex-color (slot-value planet 'color)
				   :blend :additive)))))

(defparameter *default-system-images* 
  '("aquastar.png" "bluestar.png" "flarestar.png" "greenstar.png"))

(defparameter *system-images* (image-set "system" 9))

(defparameter *full-system-images* (append *default-system-images* *system-images*))

(defparameter *system-number* 0)

(defun random-system-name ()
  (format nil "~A ~A~A" 
	  (random-choose '(U U U U U B B N))
	  (+ 284 (incf *system-number*))
	  (random-choose '(+ + -A -B -P -X))))
  
(defvar *galaxy* nil)
(defparameter *galaxy-size* 10000)
(defparameter *galaxy-radius* (/ *galaxy-size* 2))
(defparameter *minimum-galaxy-size* 800)

(defclass galaxy (sector)
  ((horizontal-scrolling-margin :initform 1/3)
   (vertical-scrolling-margin :initform 1/3)
   (window-scrolling-speed :initform 1.25)
   (zoom :initform 1.0)
   (movement-cost :initform 0.0076)
   (help-string :initform "[Spacebar/JMenu] Open menu   [Arrows/Numpad/Joystick] Move   [Shift/JFire] Engage    [Tab/Return/JAltFire] Scan")))

(defmethod exit ((galaxy galaxy)) nil)

(defun galaxy-coordinates ()
  (multiple-value-bind (x y) (center-point (slot-value *galaxy* 'cursor))
    (values x y)))

(defmethod fill-square ((sector sector) x y size 
			&key (count 100) classes)
  (dotimes (n count)
    (add-node sector (make-instance (random-choose classes))
	      (+ x (random size))
	      (+ y (random size)))))

(defmethod fill-donut ((sector sector) x y size 
		       &key (count 100) classes hole)
  (let ((hole2 (or hole (/ size 5))))
    (dotimes (n count) 
      (multiple-value-bind (zx zy)
	  (step-coordinates x y 
			    (random (* 2 pi))
			    (+ hole2 (random (- size hole2))))
	(add-node sector (make-instance (random-choose classes)) zx zy)))))

(defmethod fill-circle ((sector sector) x y radius 
			&key (count 20) classes)
  (dotimes (n count)
    (multiple-value-bind (x0 y0) 
	(step-coordinates x y (random (* pi 2)) (random radius))
      (add-node sector (make-instance (random-choose classes)) x0 y0))))
	      
(defparameter *sector-depth* 2)
(defparameter *galaxy-depth* 2)

(defmethod flood ((sector sector) x y radius &key classes (depth *sector-depth*) (count 5))
  (prog1 sector
    (when (plusp depth)
	(dotimes (n 5)
	  (multiple-value-bind (x0 y0) 
	      (step-coordinates x y (random (* pi 2)) (random (cfloat (/ radius 2.6))))
	    (let ((new-radius (+ *minimum-galaxy-size* (random (cfloat (/ radius 2)))))
		  (*cluster-heat* (+ *cluster-heat* (random 3))))
	      (fill-circle sector x0 y0 radius :count count :classes classes)
	      (flood sector x0 y0 new-radius :count count :classes classes :depth (- depth 1))))))))

(defmethod entry-point ((galaxy galaxy))
  (values (/ *galaxy-size* 3) (/ *galaxy-size* 3)))

(defvar *boss-x* 0)
(defvar *boss-y* 0)

(defmethod cheat ((galaxy galaxy))
  (credit (ship) 10000))

(defmethod initialize-instance :after ((galaxy galaxy) &key)
  (multiple-value-bind (ex ey) (entry-point galaxy)
    (multiple-value-bind (mx my) (random-encounter-coordinates ex ey 2500)
      (set-mission-coordinates mx my)
      (setf *boss-x* mx *boss-y* my)
      (roger :all-systems-nominal)
      (roger :beacon)
      (roger :mail-1)
      (move-window-to galaxy 0 0)
      (setf *current-sector* galaxy)
      (setf *galaxy* galaxy)
      (bind-event galaxy '(:f9) 'cheat) 
      (bind-event galaxy '(:raw-joystick 999 :button-down) 'engage) 
      (bind-event galaxy '(:tab) 'use-scanner)
      (bind-event galaxy '(:return) 'use-scanner)
      (bind-event galaxy '(:raw-joystick 1 :button-down) 'use-scanner)
      (bind-event galaxy '(:space) 'open-menu)
      (bind-event galaxy '(:raw-joystick 0 :button-down) 'open-menu)
      ;; (bind-event galaxy '(:r :control) 'reset-game)
      ;; (bind-event galaxy '(:q :control) 'quit-game)
      (begin-galaxy-music)
      (resize galaxy *galaxy-size* *galaxy-size*)
      (fill-square galaxy 0 0 *galaxy-size* :count 800 :classes '(star))
      (fill-donut galaxy ex ey (- *galaxy-size* 1000) :count 100 :classes '(single-patrol double-patrol triple-patrol bomber-patrol) :hole 1100)
      ;; (fill-square galaxy 500 500 (- *galaxy-size* 600) :count 100 :classes '(single-patrol double-patrol triple-patrol bomber-patrol))
      (fill-donut galaxy ex ey (- *galaxy-size* 50) :hole 500 :count 100 :classes '(anomalous-cloud))
      (fill-donut galaxy ex ey (- *galaxy-size* 50) :hole 700 :count 40 :classes '(rogue-planet))
      (add-node galaxy (make-instance 'canaz-icon) *mission-x* *mission-y*)
      (add-node galaxy (make-instance 'bomber-patrol) (+ *mission-x* 150) (+ *mission-y* 200))
      (add-node galaxy (make-instance 'double-patrol) *mission-x* *mission-y*)
      (flood galaxy *galaxy-radius* *galaxy-radius* *galaxy-radius* :depth 2 :classes '(star))
      (let ((cursor (make-instance 'cursor)))
	(add-node galaxy cursor)
	(set-cursor galaxy cursor)
	(move-cursor-to-entry-point galaxy)
	(multiple-value-bind (x y) (galaxy-coordinates)
	  (move-window-to galaxy (- x (/ *width* 2))
			  (- y (/ *height* 2)))
	  (add-node galaxy (make-instance 'starbase)
		    (+ x (* (random-choose '(-1 1)) (+ 400 (random 500.0))))
		    (+ y (* (random-choose '(-1 1)) (+ 400 (random 500.0))))))
	(multiple-value-bind (sx sy) (center-point (find-starbase))
	  (set-mission-coordinates sx sy))
	(follow-with-camera galaxy (cursor))))))
  
  (defparameter *zoom* nil)
  
(defmethod toggle-zoom ((galaxy galaxy)) 
  (setf *zoom* (if *zoom* nil t)))

(defmethod draw :before ((galaxy galaxy))
  (xelf::project-window galaxy))

(defmethod visit :before ((galaxy galaxy))
  (resume-galaxy-music)
  (xelf::clear-deleted-objects galaxy))

(defmethod update ((galaxy galaxy)) 
  (xelf::update-window-movement galaxy)
  (update-galaxy-music)
  (with-slots (objects) galaxy
    (loop for object being the hash-keys of objects do
      (when (xelfp object)
	(update (find-object object))))))

(defmethod update :after ((galaxy galaxy))
  (explode-when-empty (ship)))

(defmethod draw ((galaxy galaxy))
  (multiple-value-bind (top left right bottom) (window-bounding-box galaxy)
    (with-slots (objects) galaxy
      (loop for object being the hash-keys of objects do
	;; only draw onscreen objects
	(when (and (xelfp object)
		   (or *zoom* (colliding-with-bounding-box-p (find-object object) top left right bottom)))
	  (draw (find-object object)))))))

(defparameter *beacon-ring-radius* 100)
(defparameter *beacon-size* 10)

(defmethod draw-beacon ((galaxy galaxy))
  (let ((cursor (slot-value galaxy 'cursor)))
    (multiple-value-bind (mx my) (mission-coordinates)
      (multiple-value-bind (cx cy) (center-point cursor)
	(let ((heading (heading-to-objective cursor))
	      (distance (distance-to-objective cursor)))
	  (multiple-value-bind (x y) (step-coordinates cx cy heading (min *beacon-ring-radius* distance))
	    (draw-textured-rectangle-* x y 0 *beacon-size* *beacon-size* (find-texture "triangle.png")
				       :angle (+ -90 (heading-degrees heading)) :blend :alpha :opacity 0.9 
				       :vertex-color "magenta")
	    (draw-string (format nil "~A" (truncate distance)) (+ x (units 0.5)) (+ y (units 0.5)) 
			 :color "cyan" :font *hud-font*)))))))

(defmethod draw :after ((galaxy galaxy))
  (draw-textured-rectangle-* 0 0 0 *galaxy-size* *galaxy-size* (find-texture "cloudy.png") :blend :additive :opacity 0.01)
  (draw-modeline galaxy)
  (set-blending-mode :alpha)
  (draw-beacon galaxy))

(defun surface-class-name (surface-type)
  (let ((type (symbol-name surface-type)))
    (intern (string-upcase (concatenate 'string type "-surface")) (find-package :skyw0r))))

(defun planet-class-name (planet-type)
  (let ((type (symbol-name planet-type)))
    (intern (string-upcase (concatenate 'string type "-planet")) (find-package :skyw0r))))



