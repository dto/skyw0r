(in-package :skyw0r)

(defvar *mission-1-completed-p* nil)

(defclass special-planet-link (link)
  ((sector-class :initform 'degenerate-planet)
   (image :initform "spectron-cannon.png")))

(defclass canaz-icon (link)
  ((sector-class :initform 'canaz-encounter)
   (image :initform "vomac-gateway.png")))

(defmethod find-description ((canaz-icon canaz-icon))
  "CONVOY")

(defclass canaz-encounter (flight)
  ((sky :initform "sky-6.png")))

(defmethod exit :around ((canaz-encounter canaz-encounter))
  (if (find-enemies)
      (roger-error :no-exit)
      (call-next-method)))

(defclass canaz (enemy)
  ((armor :initform 80)
   (image :initform "boss.png")
   (image-scale :initform 2.4)))

(defmethod collide ((canaz canaz) (bullet bullet)) 
  (destroy bullet)
  (roger :weapon-ineffective))

(defmethod collide ((bullet bullet) (canaz canaz)) 
  (destroy bullet)
  (roger :weapon-ineffective))

(defmethod run :after ((canaz canaz))
  (multiple-value-bind (cx cy) (center-point canaz)
    (when (< (distance-to-ship canaz) 1000)
      (percent-of-time 0.15 (add-node (current-sector) (make-instance 'corruptor) cx cy 0))))
  (percent-of-time 3 (attack canaz)))

(defmethod explode :after ((canaz canaz))
  (setf *mission-1-completed-p* t)
  (multiple-value-bind (x y) (location (find-starbase))
    (set-mission-coordinates x y)))

(defmethod initialize-instance :after ((canaz-encounter canaz-encounter) &key)
  (setf (slot-value canaz-encounter 'terrain) "terrain-8.png")
  (add-particle canaz-encounter (make-instance 'planetar :image "planet-1.png") 
		(+ (/ *void-size* 2) 20000) (+ (/ *void-size* 2) -14000) (/ *void-size* 2.2))
  (dotimes (n 30)
    (add-particle canaz-encounter (make-instance 'globe) (random *void-size*) (random *void-size*) (random *void-size*)))
  (dotimes (n 500)
    (add-particle canaz-encounter (make-instance 'stardust) (random *void-size*) (random *void-size*) (random *void-size*)))
  (add-node canaz-encounter (make-instance 'canaz) (/ *flight-size* 2) (/ *flight-size* 5))
  (dotimes (n 4)
    (add-node canaz-encounter (make-instance 'yasichi) (random *flight-size*) (random *flight-size*))))

(defmethod visit :after ((canaz-encounter canaz-encounter))
  (roger :unrecognized))

