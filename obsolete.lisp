;; ;;; Disable mouse editing

;; (defun set-buffer-bubble (bubble)
;;   (setf (field-value :bubble (current-buffer)) bubble))

;; (define-method handle-point-motion skyw0r (x y))
;; (define-method press skyw0r (x y &optional button))
;; (define-method release skyw0r (x y &optional button))
;; (define-method tap skyw0r (x y))
;; (define-method alternate-tap skyw0r (x y))

;; (define-method quit-game skyw0r ()
;;   (shut-down)
;;   (quit))

;; ;;; Various toggles

;; ;; (define-method toggle-red-green-color-blindness skyw0r ()
;; ;;   (drop (cursor) 
;; ;; 	(new 'bubble 
;; ;; 	     (if *red-green-color-blindness*
;; ;; 		 "Red/green color blindness support ON. Full effect requires game reset (Control-R)."
;; ;; 		 "Red/green color blindness support OFF. Full effect requires game reset (Control-R)."))))

;; (defvar *music-toggled* nil)

;; ;; (define-method toggle-music skyw0r ()
;; ;;   (setf *music-toggled* t)
;; ;;   (if (sdl-mixer:music-playing-p)
;; ;;       (halt-music)
;; ;;       (play-music (random-choose *soundtrack*) :loop t)))

;; (define-method help skyw0r () 
;;   (switch-to-buffer (help-buffer)))

;; (define-method regenerate skyw0r () (reset-level))

;; (define-method toggle-joystick skyw0r ()
;;   (setf *joystick-enabled* (if *joystick-enabled* nil t))
;;   (drop (cursor) 
;; 	(new 'bubble 
;; 	     (if *joystick-enabled* 
;; 		 "Joystick support on."
;; 		 "Joystick support off."))))
      
;; (define-method toggle-pause skyw0r ()
;;   (when (not %paused)
;;     (drop (cursor) 
;; 	  (new 'bubble 
;; 	       "Game paused. Press Control-P to resume play.")))
;;   (transport-toggle-play self)
;;   (when (not %paused)
;;     (loop for thing being the hash-keys of %objects do
;;       (when (bubblep thing) (destroy thing)))))

;; (define-method reset-game skyw0r (&optional (level 1))
;;   (begin-game level))

;; (define-method draw skyw0r ()
;;   (buffer%draw self)
;;   (when (xelfp %bubble) (draw %bubble)))

;; (eval-when (:load-toplevel) 
;;   (setf *default-texture-filter* :nearest)
;;   (setf *use-antialiased-text* nil)
;;   (setf *current-directory*
;; 	(make-pathname
;; 	 :directory (pathname-directory #.#P"./"))))
  
;; ;;; Title screen

;; (define-buffer title 
;;   (quadtree-depth :initform 4)
;;     (background-image :initform "title.png"))

;; (define-method start-playing title ()
;;   (begin-game 1))

;; ;;; Ending screen

;; (defparameter *ending-scroll-speed* 0.4)

;; (define-buffer ending-screen
;;   (width :initform 1280)
;;   (height :initform 720)
;;   (background-color :initform "black"))

;; (define scroll :image "ending.png")

;; (define-method update scroll ()
;;   (when (plusp %y)
;;     (move-toward self :up *ending-scroll-speed*)))

;; (defun show-ending ()
;;   (switch-to-buffer (new 'ending-screen))
;;   (let ((scroll (new 'scroll)))
;;     (insert scroll 0 720)
;;     (resize scroll 1280 720))
;;   (play-music "theme" :loop t))

;; ;;; Help screen

;; (define-buffer help-screen
;;   (game :initform nil)
;;   (background-image :initform "help.png"))

;; (define-method resume-playing help-screen ()
;;   (sleep 0.2) ;; allow time for human to remove finger from spacebar
;;   (switch-to-buffer %game))

;; (defun help-buffer ()
;;   (let ((buffer (new 'help-screen)))
;;     (setf (field-value :game buffer) (current-buffer))
;;     (bind-event buffer '(:h :control) :resume-playing)
;;     (bind-event buffer '(:escape) :resume-playing)
;;     (bind-event buffer '(:space) :resume-playing)
;;     buffer))


