(in-package :skyw0r)

(defresource "munch1.wav" :volume 80)
(defresource "munch2.wav" :volume 80)

(defparameter *munch-sounds* '("munch1.wav" "munch2.wav"))

;;; Planets

(defparameter *planet-tags* '(:heat :pressure :lightning :wind :cold :gravity :radiation))

(defclass basic-planet (flight)
  ((terrain :initform "white")
   (described-p :initform nil)
   (surface-type :initform 'rock)
   (surface-sector :initform nil)
   (surface-class :initform 'rock-surface)
   (hazards :initform nil :accessor hazards :initarg :hazards)
   (nodes :initform (list (button 'land-on-surface) (button 'ascend)))
   (sky :initform (random-choose (image-set "sky" 5)))
   (help-string :initform "[Spacebar] Fire missile    [Escape] Ascend to space    [Control-D] Descend to surface")))

(defmethod find-description ((basic-planet basic-planet))
  (let ((key (make-keyword (slot-value basic-planet 'surface-type))))
    (roger-text key)))

(defmethod say-description ((basic-planet basic-planet))
  (let ((key (make-keyword (slot-value basic-planet 'surface-type))))
    (roger key)))

(defmethod describe-hazards ((basic-planet basic-planet))
  (dolist (hazard (hazards basic-planet))
    (roger hazard)))

(defmethod visit :before ((basic-planet basic-planet))
  (unless (slot-value basic-planet 'described-p)
    (setf (slot-value basic-planet 'described-p) t)
    (roger (make-keyword (slot-value basic-planet 'surface-type)))
    (describe-hazards basic-planet)))

(defmethod initialize-instance :after ((basic-planet basic-planet) &key)
  (bind-event basic-planet '(:escape) 'exit) 
  (bind-event basic-planet '(:d :control) 'land)
  (dotimes (n 200) 
    (add-particle basic-planet (make-instance 'stardust) (random *void-size*) (random *void-size*) (random *void-size*))))

(defmethod make-surface ((basic-planet basic-planet))
  (with-fields (surface-type surface-sector surface-class) basic-planet
    (setf surface-class (surface-class-name surface-type))
    (setf surface-sector (make-instance surface-class))
    ;; propagate hazards from sky
    (setf (hazards surface-sector)
	  (hazards basic-planet))))
    
(defmethod can-land-p ((basic-planet basic-planet)) t)

(defmethod land ((basic-planet basic-planet) &optional x0 y0)
  (with-fields (surface-sector) basic-planet
    (when (null surface-sector)
      (make-surface basic-planet))
    (set-exit-sector surface-sector basic-planet)
    (visit surface-sector)))

(defmethod land :around ((planet basic-planet) &optional x0 y0)
  (if (can-land-p planet)
      (call-next-method)
      (roger-error :cannot-land)))
   
(defmacro define-planet (name slot-declarations)
  (let ((name2 (planet-class-name name)))
    `(defclass ,name2 (basic-planet)
       ,(append (list (list 'terrain :initform `(random-choose (terrain-class-colors ,(make-keyword name))))
		      `(surface-type :initform ',name))
	 slot-declarations))))

(define-planet rock 
    ((hazards :initform (random-choose '(nil nil (:heat) (:lightning) (:heat :lightning))))))

(define-planet desert 
    ((hazards :initform (random-choose '((:heat) (:heat :wind) nil (:wind :lightning))))))

(define-planet ice 
    ((hazards :initform (random-choose '(nil (:cold :wind) (:cold))))))

(define-planet ocean
    ((hazards :initform (random-choose '(nil (:wind) (:wind :lightning))))))

(defmethod can-land-p ((ocean-planet ocean-planet)) nil) 

(define-planet magma 
  ((hazards :initform (random-choose '((:heat :wind) (:wind :pressure :lightning))))))

(defmethod can-land-p ((magma-planet magma-planet)) nil) 

(define-planet gas 
  ((hazards :initform (random-choose '((:wind :radiation) (:wind :lightning :radiation))))))

(defmethod can-land-p ((gas-planet gas-planet)) nil) 

(defmethod initialize-instance :after ((planet gas-planet) &key)
  (setf (slot-value planet 'terrain) (random-choose '("terrain-5.png" "sky-6.png" "terrain-1.png"))))

(define-planet plasma 
    ((hazards :initform (random-choose '(nil (:heat :wind) (:heat :radiation) (:heat :wind :lightning))))))
(defmethod can-land-p ((plasma-planet plasma-planet)) nil) 

(define-planet crystal ())

(define-planet degenerate ())

(define-enemy yasichi
  ((image :initform "yasichi-1.png")
   (image-scale :initform 2.2)
   (heading :initform (random (* 2 pi)))
   (armor :initform 40)))

(defmethod run ((yasichi yasichi))
  (incf (slot-value yasichi 'heading) 0.9)
  (when (not (sliding-p yasichi))
	(if (< (distance-to-ship yasichi) 500)
	    (or (percent-of-time 2
		  (prog1 t (slide-to yasichi (random-choose (list (window-left) (window-right))) (slot-value yasichi 'y))))
		(progn (move yasichi (heading-to-ship yasichi) 2)
		       (percent-of-time 1 (attack yasichi (random (* 2 pi))))
		       (percent-of-time 4 (when *use-sound* (play-sample "creep.wav")))))
	    (move yasichi (heading-to-ship yasichi) 3))))
 
(defmethod draw ((yasichi yasichi))
  (with-slots (x y width height heading image) yasichi
    (draw-textured-rectangle-* x y 0 width height (find-texture image) :angle heading)))

(defmethod collide ((yasichi yasichi) (ship ship)) nil)

(defparameter *mountain-speed* 2)
(defparameter *mountain-images* (image-set "mountain" 5))

(defclass mountain (particle)
  ((blend :initform :alpha)
   (horizontal-follow-speed :initform 19)
   (image :initform (random-choose *mountain-images*))))

(defmethod run ((mountain mountain))
  (with-slots (z) mountain
    (decf z (* 2 (ship-speed))))
  (collide-3d mountain (ship)))

(defmethod initialize-instance :after ((mountain mountain) &key)
  (resize mountain (random-choose '(1700 1400 1500 1800 2100 2300 1200)) (random-choose '(1000))))

(defun random-zap-sound ()
  (random-choose '("zap1.wav" "zap2.wav" "zap3.wav" "zap4.wav" "zap5.wav" "zap6.wav")))

 (defmethod wrap-z :after ((mountain mountain) offset)
  (with-slots (x) mountain
    (setf x (random *void-size*))))

(defmethod collide ((mountain mountain) (ship ship))
 (play-explosion-sound)
 (destroy mountain)
 (damage-shield ship 1000))

(defmethod initialize-instance :after ((planet degenerate-planet) &key)
  (setf (slot-value planet 'sky) "sky-3.png")
  (setf (slot-value planet 'terrain) "terrain-8.png")
  (dotimes (n 20)
    (add-particle planet (make-instance 'mountain) 
		  (random *void-size*) (- (/ *void-size* 2) 500) (random *void-size*)))
  ;; (add-node planet (make-instance 'bomber) (random *flight-size*) (random *flight-size*))
  (dotimes (n 4)
    (add-node planet (make-instance 'hyperceptor) (random *flight-size*) (random *flight-size*))))

(defclass rogue-planet (link)
  ((image :initform "question.png")
   (sector-class :initform (random-choose '(orange-planet fungus-planet purple-planet crystal-planet ice-planet cluster)))))

(define-planet unknown ())

(define-planet fungus 
  ((sky :initform "sky-7.png")))

(defmethod initialize-instance :after ((planet fungus-planet) &key)
  (setf (slot-value planet 'terrain) "terrain-7.png"))

;;; Functions for massaging raw Xelf plasma fractal output

(defun process-array (array function) 
  (let ((width (array-dimension array 0))
	(height (array-dimension array 1))
	(result (copy-tree array)))
    (dotimes (i width)
      (dotimes (j height)
	(setf (aref result i j)
	      (funcall function (aref array i j)))))
    result))

(defun array-maximum (array)
  (let ((max 0))
    (process-array array 
		   #'(lambda (x)
		       (prog1 x
			 (when (> x max)
			   (setf max x)))))
    max))

(defun array-minimum (array)
  (let ((min 0))
    (process-array array 
		   #'(lambda (x)
		       (prog1 x
			 (when (< x min)
			   (setf min x)))))
    min))

(defun array-range (array)
  (- (array-maximum array)
     (array-minimum array)))

(defun offset (array value)
  (process-array array #'(lambda (x)
			   (+ x value))))

(defun normalize (array)
  (let ((based-array (offset array (- 0 (array-minimum array))))
	(range (array-range array)))
    (process-array based-array #'(lambda (x) (/ x range)))))

(defun colorize-plasma (plasma colors)
  (let ((count (length colors)))
    (labels ((choose-color (value)
	       (or (nth (truncate (/ value (/ 1 count))) colors)
		   (first colors))))
      (process-array (normalize plasma) #'choose-color))))

;;; Planet surface with mining rover and combat/exploration

(defparameter *planet-terrain-unit* (units 3))
(defparameter *planet-terrain-width-in-units* 200)
(defparameter *planet-terrain-height-in-units* 200)
(defparameter *planet-terrain-width* (* *planet-terrain-unit* *planet-terrain-width-in-units*))
(defparameter *planet-terrain-height* (* *planet-terrain-unit* *planet-terrain-height-in-units*))

(defun planet-landing-site () 
    (values (+ (/ *planet-terrain-width* 2) 
	       (* (random-choose '(-1 1))
		  (random 4000)))
	    (+ (/ *planet-terrain-height* 2)
	       (* (random-choose '(-1 1))
		  (random 4000)))))

(defun planet-terrain-center () 
  (values (/ *planet-terrain-width* 2)
	  (/ *planet-terrain-height* 2)))

(defun make-surface-plasma (width height &key (graininess 1.0))
  (normalize (render-plasma width height :graininess graininess)))

(defun make-terrain-data ()
  (make-surface-plasma (truncate (/ *planet-terrain-width* *planet-terrain-unit*))
		       (truncate (/ *planet-terrain-height* *planet-terrain-unit*))
		       :graininess (random-choose '(0.5 0.8 1.0 1.0 1.1 1.2 1.5))))

(defclass lander (texel)
  ((image :initform "lander.png")
   (image-scale :initform 4)
   (nodes :initform (list (folder :lander
				  (button 'field-repair)
				  (button 'launch))))))

(defparameter *maximum-armor* 100)

(defclass rover (cursor)
  ((image :initform "rover.png")
   (image-scale :initform 0.65)
   (max-acceleration :initform 0.03)
   (max-speed :initform 1)
   (armor :initform *maximum-armor*)
   (nodes :initform (list (folder :rover
				  (button 'grab)
				  (button 'activate-extension)
				  (button 'disembark)
				  (button 'embark))))))

(defmethod damage-armor ((rover rover) &optional (points 1))
  (with-slots (armor) rover
    (decf armor points)
    (play-whack-sound)
    (unless (plusp armor)
      (explode rover))))

(defparameter *mineral-scanning-range* 500)

(defclass pickaxe (texel)
  ((image :initform "pickaxe.png")
   (hidden-p :initform (percent-of-time (random-choose '(30 60 90)) t) :accessor hidden-p :initarg :hidden-p)
   (image-scale :initform 1.4)))

(defmethod within-scanner-range-p ((pickaxe pickaxe))
  (< (distance-to-cursor pickaxe) *mineral-scanning-range*))

(defmethod draw :around ((pickaxe pickaxe))
  (when (or (scanning-p)
	    (not (hidden-p pickaxe)))
    (call-next-method)))

(defmethod collide ((rover rover) (pickaxe pickaxe))
  (play-sample (random-zap-sound))
  (add-material (ship) (random-material) (+ 0.1 (random 2.4)))
  (destroy pickaxe))

(defparameter *noise-images* (image-set "noise" 4))

(defclass surface (sector)
  ((terrain-data :initform (make-terrain-data) :accessor terrain-data)
   (terrain-color-data :initform nil :accessor terrain-color-data)
   (terrain-colors :initform nil :accessor terrain-colors)
   (terrain-noise :initform (random-choose *noise-images*))
   (material-density :initform (random-choose '(40 70 90)) :accessor material-density)
   (material-types :initform *materials* :accessor material-types)
   (horizontal-scrolling-margin :initform 1/3)
   (vertical-scrolling-margin :initform 1/3)
   (window-scrolling-speed :initform 2.5)  
   (movement-cost :initform 0.0001)))

(defmethod update :after ((surface surface))
  (when (member :heat (hazards surface))
    (percent-of-time 10 (damage-shield (ship) 35))))

(defun find-lander () (first (find-instances (current-buffer) 'lander)))

(defmethod touching-lander-p ((rover rover))
  (colliding-with-p rover (find-lander)))

(defmethod heading-to-lander ((rover rover))
  (heading-between rover (find-lander)))

(defparameter *surface-beacon-size* 12)

(defmethod draw :after ((rover rover))
  (unless (member :lightning (hazards (current-sector)))
    (multiple-value-bind (cx cy) (center-point rover)
      (multiple-value-bind (tx ty) (step-coordinates cx cy (heading-to-lander rover) (units 5))
	(draw-textured-rectangle-* tx ty 0 *surface-beacon-size* *surface-beacon-size* (find-texture "triangle.png") 
				   :vertex-color "magenta" :angle (+ -90 (heading-degrees (heading-to-lander rover))))
	(draw-textured-rectangle-* (+ tx 2) (+ ty 2) 0 *surface-beacon-size* *surface-beacon-size* (find-texture "triangle.png") 
				   :vertex-color "yellow" :angle (+ -90 (heading-degrees (heading-to-lander rover))))
	(draw-textured-rectangle-* (+ tx 4) (+ ty 4) 0 *surface-beacon-size* *surface-beacon-size* (find-texture "triangle.png") 
				   :vertex-color "green" :angle (+ -90 (heading-degrees (heading-to-lander rover))))))))

(defmethod exit :around ((surface surface))
  (if (touching-lander-p (cursor))
      (call-next-method)
      (roger-error :must-return)))

(defmethod find-sky ((surface surface))
  (exit-sector surface))

(defmethod terrain-background-bounding-box ((surface surface) i j)
  (let ((unit *planet-terrain-unit*))
    (values (* j unit) (* i unit)
	    (* (1+ i) unit)   (* (1+ j) unit))))

(defmethod initialize-instance :after ((surface surface) &key)
  (stop-engine-sound)
  (bind-event surface '(:raw-joystick 999 :button-down) 'engage) 
  (bind-event surface '(:escape) 'exit) 
  (bind-event surface '(:space) 'open-menu)
  (bind-event surface '(:tab) 'use-scanner)
  (bind-event surface '(:return) 'use-scanner)
  (bind-event surface '(:raw-joystick 1 :button-down) 'use-scanner)
  (bind-event surface '(:raw-joystick 0 :button-down) 'open-menu)
  (setf (terrain-color-data surface)
	(colorize-plasma (terrain-data surface) (terrain-colors surface)))
  (resize surface *planet-terrain-width* *planet-terrain-height*)
  (fill-square surface (/ *planet-terrain-width* 2) (/ *planet-terrain-height* 2) (/ *planet-terrain-height* 2)
  	       :classes '(pickaxe) :count (material-density surface))
  (flood surface (/ *planet-terrain-width* 2) (/ *planet-terrain-height* 2) (/ *planet-terrain-height* 2)
	 :classes '(pickaxe) :count (ceiling (/ (material-density surface) 12)))
  (fill-donut surface (/ *planet-terrain-width* 2) (/ *planet-terrain-height* 2) (/ *planet-terrain-height* 2)
  	       :hole 700 :classes '(pickaxe) :count (material-density surface))
  (let ((rover (make-instance 'rover)))
    (multiple-value-bind (x0 y0) (planet-landing-site)
      (add-node surface (make-instance 'lander) (- x0 (units 25.5)) (- y0 (units 13)))
      (add-node surface rover x0 y0)
      (set-cursor surface rover))
    (follow-with-camera surface rover)))
  
(defmethod draw ((surface surface))
  (with-slots (objects terrain-noise window-x window-y window-z) surface
    (project-orthographically t)
    (transform-window :x window-x :y window-y :z window-z)
    (let* ((u *planet-terrain-unit*)
	   (uw *planet-terrain-width-in-units*)
	   (uh *planet-terrain-height-in-units*)
	   (w *planet-terrain-height*)
	   (h *planet-terrain-height*)
	   ;; screen dimensions, in units
	   (swu (1+ (ceiling (/ *width* u))))
	   (shu (1+ (ceiling (/ *height* u))))
	   ;; plasma data coordinate, in units
	   (x0u (floor (/ (window-x) u)))
	   (y0u (floor (/ (window-y) u)))
	   (x0 (* u x0u))
	   (y0 (* u y0u)))
      (dotimes (i swu)
	(dotimes (j shu)
	  (multiple-value-bind (top left right bottom) 
	      (terrain-background-bounding-box surface i j)
	    (draw-box (+ x0 (* i u))
		      (+ y0 (* j u)) u u
		      :color (aref (terrain-color-data surface) 
				   (min (1- uw) (+ x0u i))
				   (min (1- uh) (+ y0u j)))))))
      (draw-textured-rectangle-* 0 0 0 w h (find-texture terrain-noise) 
				 :blend :multiply :opacity 0.8))
    (xelf::draw-object-layer surface)
    (draw-modeline surface)))

(defmacro define-surface (name slot-declarations)
  (let ((name2 (surface-class-name name)))
    `(defclass ,name2 (surface)
     ,(append (list `(surface-type :initform ',name)
		    `(terrain-colors :initform (terrain-class-colors ',name)))
       slot-declarations))))

;;; Robots and various creatures

(defparameter *creature-images* '("humanoid.png" "biclops.png" "gond.png" "toxic-hazard.png" "excretor.png" "leech.png" "wormhole.png"  "jelly.png" "lymphocyte.png"))

(defparameter *robot-images* '("shocker.png" "xiocond.png" "probe.png"))

(defclass creature (texel)
  ((image :initform (random-choose *creature-images*))
   (species :initform (xelf::make-uuid))
   (mood :initform (random-choose '(:docile :hostile :vegetative :intelligent)))
   (image-scale :initform (random-choose '(1.2 1.4 1.6 2 2.5)))))

(defmethod update ((creature creature))
  (with-slots (heading) creature
    (move creature heading 0.32)
    (percent-of-time 0.01 (setf heading (random (* 2 pi))))))

(defmethod update :after ((creature creature))
  (when (and (scanning-p)
	     (within-scanner-range-p creature))
    (find-scanner-data creature)))

(defmethod find-scanner-data :before ((creature creature))
  (unless (scanned-p creature)
    (roger (slot-value creature 'mood))))

(defmethod draw :after ((creature creature))
  (when (and (scanner-display-p (current-sector))
	     (show-scanner-data-p creature))
    (multiple-value-bind (x y) (location creature)
      (draw-string (format nil "~A" (find-description creature))
		   x (- y (font-height *hud-font*) 2)
		   :color "white" :font *hud-font*))))

(defmethod find-description ((creature creature))
  (with-slots (image) creature
    (concatenate 'string
		 (pretty-string (slot-value creature 'mood))
		 " "
		 (subseq image 0 (- (length image) 4)))))

(defclass robot (texel) 
  ((armor :initform 12 :accessor armor)
   (image-scale :initform (random-choose '(1.0 1.1 1.2 1.3 1.4 1.5 1.6)))))

(defclass biclops (robot)
  ((image :initform "biclops.png")
   (image-scale :initform 0.5)
   (speed :initform 0.8)))

(defmethod update ((biclops biclops))
  (with-slots (heading speed) biclops
    (move biclops heading speed)
    (percent-of-time 0.01 (setf heading (random (* 2 pi))))))

;;; Rocky planets

(define-surface rock 
  ((material-density :initform (random-choose '(40 50 70 80)))))

;; (defmethod initialize-instance :after ((rock rock-surface) &key)
;;   (dotimes (n 100)
;;     (add-node rock (make-instance (random-choose '(biclops biclops))) (random *planet-terrain-width*) (random *planet-terrain-height*))))

;;; Desert planet

(define-surface desert 
  ((terrain-noise :initform "noise-1.png")))

;;; Ice planets

(define-surface ice 
  ((material-density :initform (random-choose '(40 50 80)))
   (terrain-noise :initform (random-choose '("noise-3.png" "noise-4.png")))))

;; (defmethod initialize-instance :after ((ice ice-surface) &key)
;;   (dotimes (n 100)
;;     (add-node ice (make-instance 'creature) (random *planet-terrain-width*) (random *planet-terrain-height*))))

(define-surface ocean ())

(define-surface fungus 
  ((terrain-noise :initform "noise-2.png")))

(defmethod initialize-instance :after ((fungus fungus-surface) &key)
  (roger :lifeforms)
  (dotimes (n 120)
    (add-node fungus (make-instance 'creature) (random *planet-terrain-width*) (random *planet-terrain-height*))))

(define-surface magma ())
(define-surface gas ())
(define-surface plasma ())
(define-surface crystal ())

(define-surface degenerate
    ((terrain-noise :initform (random-choose '("noise-3.png" "noise-4.png")))))

(define-surface unknown ())




