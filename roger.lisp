(in-package :skyw0r)

(defparameter *roger-data* (eval-when (:compile-toplevel :execute) (first (read-sexp-from-file "screenplay.sexp"))))

(defvar *message* nil)
(defvar *message-clock* 0)
(defparameter *message-time* 350)

(defun show-message (text &optional (time *message-time*))
  (setf *message-clock* time)
  (setf *message* text))

(defun error-message (text)
  (play-error-sound)
  (show-message text))

(defun update-message-clock ()
  (when (plusp *message-clock*)
    (decf *message-clock*))
  (when (zerop *message-clock*)
    (setf *message* nil)))

(defun clear-message ()
  (setf *message-clock* 0)
  (setf *message* nil))

(defun roger-data (key) (find key *roger-data* :key #'first))
(defun roger-text (key) (second (roger-data key)))
(defun roger-sample (key) (concatenate 'string "_" (third (roger-data key))))

(defparameter *roger-keys* (mapcar #'first *roger-data*))
(defvar *roger-key* nil)
(defvar *roger-channel* nil)
(defvar *roger-queue* nil)

(defun roger-speaking-p () *roger-channel*)
(defun roger-silent-p () (null *roger-channel*))

(defun roger-reset ()
  (when *roger-channel*
    (halt-sample *roger-channel*))
  (setf *roger-channel* nil)
  (setf *roger-queue* nil))

(defun roger-say (key)
  (setf *roger-key* key)
  (setf *roger-channel* 
	(play-sample (roger-sample key)))
  (show-message (roger-text key)))

(defun roger-queue (key)
  (when (not (or (eq key (first *roger-queue*))
		 (eq key *roger-key*)))
    (setf *roger-queue* (append *roger-queue* (list key)))))

(defun roger-unqueue ()
  (when *roger-queue*
    (pop *roger-queue*)))

(defun roger-callback (sample)
  (when (and (roger-speaking-p)
	     (= sample *roger-channel*))
    (setf *roger-channel* nil)
    (setf *roger-key* nil)
    (let ((next (roger-unqueue)))
      (when next (roger next)))))

(defun roger-update ()
  (when (and (roger-speaking-p)
	     (null (sdl-mixer:sample-playing-p *roger-channel*)))
    (roger-callback *roger-channel*)))

(defun roger (key &optional force)
  (if (or force (roger-silent-p))
      (progn (when force (roger-reset))
	     (roger-say key))
      (roger-queue key)))

(defun roger-error (key)
  (error-message (roger-text key))
  (roger key))
