(in-package :skyw0r)

(defun derange (things)
  (let ((len (length things))
	(things2 (coerce things 'vector)))
    (dotimes (n len)
      (rotatef (aref things2 n)
	       (aref things2 (random len))))
    (coerce things2 'list)))

(defvar *current-sector* nil)
(defun current-sector () *current-sector*)

(defclass sector (buffer) 
  ((background-color :initform "midnight blue")
   (scanner-display-p :initform t :accessor scanner-display-p)
   (timer :initform 0 :accessor timer)
   (hazards :initform nil :accessor hazards)
   (nodes :initform nil :initarg :nodes :accessor nodes)
   (help-string :initform "[Spacebar/JMenu] Open Menu   [Arrows/Numpad/Joystick] Move   [Shift/JFire] Engage   [Tab/Return/JAltFire] Scan  [Escape] Exit sector")
   (movement-cost :initform 0.0003 :accessor movement-cost)
   (link :initform nil :accessor link)
   (exit-sector :initform nil :accessor exit-sector)
   (generated-p :initform nil :accessor generated-p)
   (system-name :initform nil :accessor system-name)
   (cursor-class :initform 'cursor :accessor cursor-class)
   (cursor :initform nil)))

;;; Camera control

(defmethod glide-follow ((self sector) object)
  (with-local-fields 
    (with-fields (window-x window-y width height horizontal-scrolling-margin vertical-scrolling-margin) self
      (let ((margin-x (* horizontal-scrolling-margin *width*))
	    (margin-y (* vertical-scrolling-margin *height*)))
	(multiple-value-bind (object-x object-y)
	    (center-point object)
	  (multiple-value-bind (tx ty) 
	      (values object-x object-y)
		;; yes. recenter.
		(glide-window-to self
				 (truncate (max 0
						(min (- width *width*)
						     (- tx
							(truncate (/ *width* 2))))))
				 (truncate (max 0 
				      (min (- height *height*)
					   (- ty 
					      (truncate (/ *height* 2)))))))))))))

(defmethod update-window-glide ((self sector))
  (with-fields (window-x window-x0 window-y window-y0 window-scrolling-speed) self
    (labels ((nearby (a b)
	       (> window-scrolling-speed (abs (- a b))))
	     (jump (a b)
	       (let ((speed (if (and 
				 (numberp window-x0)
				 (numberp window-y0)
				 (not (nearby window-x window-x0))
				 (not (nearby window-y window-y0)))
				;; tamp down diagonal scrolling speed a bit
				(/ window-scrolling-speed (sqrt 2))
				window-scrolling-speed)))
		 (if (< a b) speed (- speed)))))
      (when (and window-x0 window-y0)
	(if (nearby window-x window-x0)
	    (setf window-x0 nil)
	    (incf window-x (jump window-x window-x0)))
	(if (nearby window-y window-y0)
	    (setf window-y0 nil)
	    (incf window-y (jump window-y window-y0)))))))

(defmethod initialize-instance :after ((sector sector) &key)
  (bind-event sector '(:escape) 'exit)
  (bind-event sector '(:raw-joystick 0 :button-down) 'open-menu))

(defmethod find-ambient-nodes ((sector sector)) nil)

(defmethod entry-point ((sector sector)) 
  (values 100 100))

(defmethod move-cursor-to-entry-point ((sector sector))
  (multiple-value-bind (x y) (entry-point sector)
    (with-buffer sector
      (move-to (slot-value sector 'cursor) x y))))

(defmethod update :before ((sector sector))
  (update-message-clock)
  (update-timer sector)
  (roger-update))

(defmethod update :after ((sector sector))
  (when (and (holding-fire)
	     (not (timer-running-p sector)))
    (send-event (list :raw-joystick 999 :button-down)))
  (mapc #'xelf::send-to-blocks *events*)
  (setf *events* nil))

(defmethod handle-event :around ((sector sector) event)
  (if (find :raw-joystick event :test 'eq)
      (unless (timer-running-p sector)
  	(start-timer sector)
  	(call-next-method))
      (call-next-method)))

(defmethod draw :before ((sector sector)) 
  (project-orthographically sector))

(defmethod draw ((sector sector))
  (multiple-value-bind (top left right bottom) (window-bounding-box sector)
    (with-slots (objects) sector
      (loop for object being the hash-keys of objects do
	(when (and (xelfp object)
		   (or *zoom* (colliding-with-bounding-box-p (find-object object) top left right bottom)))
	  (draw (find-object object)))))))

(defmethod handle-point-motion ((self sector) x y))
(defmethod press ((self sector) x y &optional button))
(defmethod release ((self sector) x y &optional button))
(defmethod tap ((self sector) x y))
(defmethod alternate-tap ((self sector) x y))

(defmethod help-left ((sector sector))
  (units 24))

(defmethod draw-fuel ((sector sector) x y)
  (draw-string (format-fuel-string) x y :color "cyan" :font *hud-font*))

(defmethod draw-energy ((sector sector) x y)
  (draw-string (format-energy-string) x y :color "yellow" :font *hud-font*))

(defmethod draw-missiles ((sector sector) x y)
  (draw-string (format-missiles-string) x y :color "lawn green" :font *hud-font*))

(defun shield-number-color ()
  (if (plusp (slot-value (ship) 'stagger))
      (random-choose '("cyan" "white"))
      "yellow"))

(defun shield-number-background ()
  (if (plusp (slot-value (ship) 'stagger))
      "red"
      "black"))

(defun format-damage-string ()
  (let (tags)
    (when (shield-damaged-p (ship)) 
      (push 'sh tags))
    (when (radar-damaged-p (ship)) 
      (push 'ra tags))
    (when (targeting-damaged-p (ship)) 
      (push 'tr tags))
    (if tags
	(format nil "DAMAGE:~A" tags)
	" ")))

(defun format-hazard-string ()
  (let ((hazards (hazards (current-sector))))
    (if hazards (format nil "HAZARD:~A" hazards) " ")))

(defmethod draw-damage ((sector sector) x y)
  (draw-string (format-damage-string) x y :color "red" :font *hud-font*))

(defmethod draw-hazards ((sector sector) x y)
  (draw-string (format-hazard-string) x y :color "orange" :font *hud-font*))

(defmethod draw-shield ((sector sector) x y)
  (draw-box x y (font-text-width (format-shield-string) *hud-font*)
	    (font-height *hud-font*)
	    :color (shield-number-background))
  (draw-string (format-shield-string) x y :color (shield-number-color) :font *hud-font*))

(defmethod draw-credits ((sector sector) x y)
  (draw-string (format-credits-string) x y :color "orange" :font *hud-font*))

(defmethod find-help-string ((sector sector))
  (slot-value sector 'help-string))

(defmethod modeline-message-string ((sector sector))
  (or *message*
      (find-help-string sector)))

(defmethod draw-modeline ((sector sector))
  (xelf:set-blending-mode :alpha)
  (multiple-value-bind (top left right bottom) (window-bounding-box sector)
    (let ((mtop (+ (window-y) *height* (units -0.7)))
	  (mleft (+ left (units 0.2))))
      (draw-box left (- mtop 3) *width* (units 1) :color "black")
      (draw-fuel sector (+ mleft (units 0.2)) mtop)
      (draw-shield sector (+ mleft (units 5.0)) mtop)
      (draw-missiles sector (+ mleft (units 9.2)) mtop)
      (draw-credits sector (+ mleft (units 13.2)) mtop)
      (draw-damage sector (+ mleft (units 8.4)) (- mtop (units 1)))
      (draw-hazards sector (+ mleft (units 8.4)) (- mtop (units 2)))
      (when *galaxy*
	(multiple-value-bind (x0 y0) (galaxy-coordinates)
	  (draw-string (format nil "X:~1,'0D Y:~1,'0D" (truncate x0) (truncate y0))
		       (+ mleft (units 17.8))
		       mtop :color "white")))
      (draw-string (modeline-message-string sector)
		   (+ (window-x) (help-left sector))
		   mtop
		   :color 
		   (if (null *message*)
		       "gray80" 
		       (random-choose '("cyan" "white")))
		   :font *hud-font*))))

(defmethod generate ((sector sector)) nil)

(defmethod generate :after ((sector sector))
  (setf (generated-p sector) t))

(defmethod find-name ((sector sector))
  (or (slot-value sector 'system-name)
      (string-downcase (class-name (find-class sector)))))

(defmethod can-visit-p ((sector sector)) t)

(defmethod visit ((sector sector))
  (setf *current-sector* sector)
  (switch-to-buffer sector))

(defmethod visit :before ((sector sector))
  (unless (generated-p sector)
    (generate sector)))

(defmethod visit :after ((sector sector))
  (start-timer sector))

(defmethod set-exit-sector ((sector sector) exit-sector)
  (if (eq sector exit-sector)
      (message "Warning: not setting sector's exit-sector to itself") 
      (setf (exit-sector sector) exit-sector)))

(defmethod set-link ((sector sector) link)
  (setf (link sector) link))

(defmethod exit ((sector sector))
  (start-timer sector)
  (visit (exit-sector sector)))

(defmethod reset-game ((sector sector))
  (begin-game))

(defmethod quit-game (sector)
  (quit))

;;; The cursor, player's avatar (ship, tank, guy)

(defparameter *scanner-timeout* (seconds 10))

(defclass cursor (texel)
  ((image :initform "ship-cursor.png")
   (scanner-timer :initform 0 :accessor scanner-timer)
   (movement-sample :initform "hyperdrive.wav" :accessor movement-sample)
   (image-scale :initform 0.4)
   (max-ddx :initform 0.05)
   (max-ddy :initform 0.05)
   (max-speed :initform 0.38)))

(defmethod begin-scanning ((cursor cursor))
  (roger :scanning)
  (play-sample "scanner.wav")
  (setf (scanner-timer cursor) *scanner-timeout*))

(defmethod update-scanner ((cursor cursor))
  (when (plusp (scanner-timer cursor))
    (decf (scanner-timer cursor))))

(defun scanning-p (&optional (cursor (cursor)))
  (when cursor
    (plusp (slot-value cursor 'scanner-timer))))

(defmethod distance-to-objective ((cursor cursor))
  (multiple-value-bind (mx my) (mission-coordinates)
    (with-slots (x y) cursor
      (distance x y mx my))))

(defmethod heading-to-objective ((cursor cursor))
  (multiple-value-bind (mx my) (mission-coordinates)
    (with-slots (x y) cursor
      (find-heading x y mx my))))

(defmethod use-scanner ((sector sector))
  (unless (scanning-p (cursor))
    (begin-scanning (cursor))))

(defresource "hyperdrive.wav" :volume 50)

(defvar *cursor-channel* nil)

(defun cursor-sound-p () *cursor-channel*)

(defun play-cursor-sound ()
  (when *use-sound*
    (when *cursor-channel* (halt-sample *cursor-channel*))
    (setf *cursor-channel* 
	  (play-sample (movement-sample (cursor)) :loop t))))

(defun stop-cursor-sound ()
  (when *cursor-channel* (halt-sample *cursor-channel*))
  (setf *cursor-channel* nil))

(defun cursor ()
  (when (current-sector)
    (slot-value (current-sector) 'cursor)))

(defmethod set-cursor ((sector sector) cursor)
  (setf (slot-value sector 'cursor) cursor))

(defsetf cursor set-cursor)

(defmethod find-selection ((cursor cursor))
  (with-slots (objects) (current-sector)
    (block finding
      (maphash #'(lambda (k v)
		   (when (and (colliding-with-p cursor (find-object k))
			      (not (eq cursor (find-object k))))
		     (return-from finding (find-object k))))
	       objects))))

(defmethod find-local-selection ((cursor cursor))
  (with-slots (objects) (current-sector)
    (let (selection)
      (maphash #'(lambda (k v)
		   (when (and (xelfp k) (colliding-with-p cursor (find-object k))
			      (not (eq cursor (find-object k))))
		     (push (find-object k) selection)))
	       objects)
      selection)))

(defparameter *cursor-thrust* 0.04)

(defun decay (x)
  (let ((z (* 0.94 x)))
    z))

(defmethod current-heading ((self cursor)) 
  (slot-value self 'heading))

(defmethod update :before ((cursor cursor))
  (update-physics cursor)
  (update-scanner cursor)
  (if (or (thrust-x cursor) (thrust-y cursor))
      (when (not (cursor-sound-p))
	(play-cursor-sound))
      (stop-cursor-sound))
  (when (or (thrust-x cursor) (thrust-y cursor))
    (burn-endurium (ship) (movement-cost (current-sector))))
  (restrict-to-buffer cursor))

(defmethod update-thrust ((self cursor))
  (with-fields (tx ty) self
    (let ((heading (current-heading self))
	  (thrust-x (thrust-x self))
	  (thrust-y (thrust-y self)))
      (setf tx (if thrust-x (* thrust-x (cos heading)) nil))
      (setf ty (if thrust-y (* thrust-y (sin heading)) nil)))))

(defmethod thrust-x ((self cursor)) 
  (destructuring-bind (horizontal vertical) (xelf::joystick-left-analog-stick)
    (when (or (member (arrow-direction) '(:left :right :upleft :downleft :upright :downright))
	      (joystick-axis-pressed-p horizontal *player-1-joystick*))
      *cursor-thrust*)))

(defmethod thrust-y ((self cursor)) 
  (destructuring-bind (horizontal vertical) (xelf::joystick-left-analog-stick)
    (when (or (member (arrow-direction) '(:up :down :upleft :downleft :upright :downright))
	      (joystick-axis-pressed-p vertical *player-1-joystick*))
      *cursor-thrust*)))

(defparameter *player-1-joystick* 0)

(defmethod stick-heading ((self cursor))
  (when (or (left-analog-stick-pressed-p *player-1-joystick*))
    (left-analog-stick-heading *player-1-joystick*)))
  
(defmethod movement-heading ((self cursor))
  (let ((dir (or 
	      (arrow-direction)
		;; restrict to 8-way
		(when (stick-heading self)
		  (or (heading-direction (stick-heading self))
		      :left)))))
    (when dir (direction-heading dir))))

(defmethod draw ((self cursor))
  (with-slots (image heading) self
    (multiple-value-bind (top left right bottom)
	(bounding-box self)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture image)
				 ;; adjust angle to normalize for up-pointing sprites 
				 :angle (+ 90 (heading-degrees heading))))))

;;; Links are objects in the map that link to another sector.

(defparameter *link-size* (units 2.3))

(defclass link (texel)
  ((sector :initform nil)
   (sector-class :initform nil)
   (parameters :initform nil)
   (title :initform (random-system-name))))

(defmethod update :after ((link link))
  (when (and (scanning-p)
	     (within-scanner-range-p link))
    (find-scanner-data link)))

(defmethod draw :after ((link link))
  (when (and (scanner-display-p (current-sector))
	     (show-scanner-data-p link))
    (multiple-value-bind (x y) (location link)
      (draw-string (format nil "~A" (find-description link))
		   x (- y (font-height *hud-font*) 2)
		   :color "white" :font *hud-font*))))

(defmethod make-sector ((link link) parameters)
  (with-slots (sector-class) link
    (apply #'make-instance sector-class parameters)))

(defmethod find-sector ((link link))
  (with-slots (sector parameters) link
    (or sector 
	(setf sector (make-sector link parameters)))))

(defmethod follow ((texel texel) exit-sector) nil)

(defmethod follow ((link link) exit-sector)
  (let ((sector (find-sector link)))
    (when (can-visit-p sector)
      (set-link sector link)
      (set-exit-sector sector exit-sector)
      (visit sector))))

(defmethod engage ((sector sector))
  (when (cursor)
    (let ((link (find-selection (cursor))))
      (when link (follow link sector)))))


