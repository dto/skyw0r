;;; skyw0r.lisp --- a mashup of 8-bit space exploration/combat games

;; Copyright (C) 2013  David O'Toole

;; Author: David O'Toole <dto@blocky.io>
;; Keywords: games

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :skyw0r)

;;; Main game generator

(defun skyw0r-test ()
  (let ((ship (make-instance 'ship)))
    (switch-to-buffer (make-instance 'map-screen))
    (current-buffer)))

(defun make-game (&optional (class 'galaxy))
  (let ((ship (make-instance 'ship)))
    (let* ((sector (make-instance class))
	   (cursor (make-instance (cursor-class sector))))
      (visit sector)
      (current-sector))))

(defun make-galaxy ()
  (let ((ship (make-instance 'ship)))
    (switch-to-buffer (make-instance 'galaxy))
    (current-buffer)))

(defun begin-game () 
  (roger-reset)
  (setf *mission-1-completed-p* nil)
  (halt-sample t)
  (stop-engine-sound)
  (at-next-update (visit (make-galaxy))))

;;; Title screen

(defclass title (buffer)
  ((quadtree-depth :initform 4)
   (background-image :initform "title.png")))

(defmethod initialize-instance :after ((title title) &key)
  (resize title *width* *height*))

(defmethod handle-event ((title title) event) 
  (when (eq :joystick (first event))
    (destructuring-bind (which button direction) (rest event)
      (setf *player-1-joystick* which)
      (begin-game))))

(defmethod tap ((title title) x y))
(defmethod scroll-tap ((title title) x y) nil)
(defmethod alternate-tap ((title title) x y) nil)

;;; Game over screen

(defclass game-over (sector)
  ((quadtree-depth :initform 4)
   (timer :initform 60)
   (background-image :initform "game-over.png")))

(defclass card (texel)
  ((image :initform "game-over.png")))

(defmethod initialize-instance :after ((game-over game-over) &key)
  (bind-event game-over '(:escape) 'reset-the-game)
  (add-node game-over (make-instance 'card) 0 0 0)
  (resize game-over *width* *height*)
  (let ((cursor (make-instance 'cursor)))
    (add-node game-over cursor 500 200 1)
    (set-cursor game-over cursor)
    (move-window-to game-over 0 0)))

(defmethod draw ((game-over game-over))
  (with-slots (objects window-x window-y window-z) game-over
    (project-orthographically t)
    (transform-window :x window-x :y window-y :z window-z)
    (xelf::draw-object-layer game-over)))

;; (defmethod draw ((game-over game-over))
;;   ;; (gl:load-identity)
;;   ;; (xelf::update-window-movement game-over)
;; ;;  (project-orthographically game-over)
;;   (multiple-value-bind (top left right bottom) (window-bounding-box game-over)
;;     (with-slots (objects) game-over
;;       (loop for object being the hash-keys of objects do
;; 	(when (and (xelfp object)
;; 		   (or *zoom* (colliding-with-bounding-box-p (find-object object) top left right bottom)))
;; 	  (draw (find-object object)))))))

(defparameter *game-over-exit-x* 732)
(defparameter *game-over-exit-y* 393)

(defmethod reset-the-game ((game-over game-over)) 
  (begin-game))

;; (defmethod draw :before ((game-over game-over))
;;   (project-orthographically game-over)
;;   (gl::clear-color 0 0 0 1)
;;   (gl::clear))

;; (defmethod visit :after ((game-over game-over))
;;   (move-window-to game-over 0 0))

;; (defmethod draw :after ((game-over game-over))
;;   (move-window-to game-over 0 0 0)
;;   (show-message "Press Escape to reset the game.")
;;   (set-blending-mode :alpha)
;;   (draw-image "game-over.png" 0 0 :z 0 :blend :alpha :opacity 1.0 :height *height* :width *width*)
;;   (draw-modeline game-over))

(defmethod update :after ((game-over game-over))
  (multiple-value-bind (x y) (center-point (cursor))
    (when (< (distance x y *game-over-exit-x* *game-over-exit-y*) 70)
      (reset-game game-over))))

(defmethod tap ((game-over game-over) x y))
(defmethod scroll-tap ((game-over game-over) x y) nil)
(defmethod alternate-tap ((game-over game-over) x y) nil)

(defun game-over-screen () (make-instance 'game-over))

(defmethod draw :after ((game-over game-over))
  (with-slots (timer) game-over
    (decf timer)
    (when (plusp timer)
      (draw-box 0 0 *width* *height* :color (random-choose '("red" "orange" "magenta" "yellow")) :alpha 0.8))))

(defvar *events* nil)

(defun skyw0r-event (event)
  (push event *events*))
  
;;; Main program 

(defun start-engine (class)
  (setf *window-title* "skyw0r v0.9 ALPHA")
  (setf *screen-width* *width*)
  (setf *screen-height* *height*)
  (setf *nominal-screen-width* *width*)
  (setf *nominal-screen-height* *height*)
  (setf *scale-output-to-window* t) 
  (setf *font-texture-scale* 2)
  (setf *font-texture-filter* :linear)
  (setf *menu* nil)
  (setf *events* nil)
  (setf *default-texture-filter* :nearest)
  (setf *use-antialiased-text* nil)
  (setf *frame-rate* +fps+)
  (setf *joystick-dead-zone* 4000)
  (setf *mission-1-completed-p* nil)
  (enable-key-repeat 30 8)
  ;; (disable-key-repeat)
  (setf *font* "sans-mono-bold-11") 
  (with-session 
    (open-project "skyw0r")
    (index-all-images)
    (index-all-samples)
    (index-pending-resources)
    (preload-resources)
    (roger-reset)
    (setf *event-handler-function* #'skyw0r-event)
    ;; (sdl-mixer:register-sample-finished #'roger-callback)
    (visit (make-game class))))

(defun skyw0r (&optional (class 'galaxy))
  (start-engine class))

;;; skyw0r.lisp ends here
