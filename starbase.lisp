(in-package :skyw0r)

(defresource "keyclick.wav" :volume 20)
(defresource "select.wav" :volume 20)
(defresource "menu.wav" :volume 20)

(defvar *tree-depth* 0)

(defmacro deeper (&rest body)
  `(let ((*tree-depth* (1+ *tree-depth*)))
     ,@body))

(defparameter *depth-gray-slope* -2)
(defparameter *depth-gray-base* 50)

(defun depth-gray (depth)
  (percent-gray (+ *depth-gray-base* (* depth *depth-gray-slope*))))

(defparameter *terminal-font* "sans-mono-16")
(defparameter *terminal-small* "sans-mono-14")
(defparameter *terminal-bold* "sans-mono-bold-16")
(defparameter *heading-font* "sans-mono-bold-16")
(defparameter *foreground* "white")
(defparameter *background* "yellow green")
(defparameter *highlight* "yellow")
(defparameter *point-color* "gray50")
(defparameter *border* "black")
(defparameter *tab-width* 20)
(defparameter *delimiter* "/")
(defparameter *root-label* *delimiter*)

(defun terminal-font-height () (font-height *terminal-font*))
(defun heading-font-height () (font-height *heading-font*))
(defun no-label-string () "<no label>")

(defvar *leaf* nil)
(defun leaf () *leaf*)
(defun set-leaf (leaf) (setf *leaf* leaf)) 

(defclass entry (texel)
  ((label :initform (no-label-string) :accessor label :initarg :label)
   (timer :initform 0 :accessor timer)
   (parent :initform nil :accessor parent :initarg :parent)
   (point :initform 0 :accessor point :initarg :point)
   (highlight-p :initform t :accessor highlight-p :initarg :highlight-p)
   (expanded-p :initform t :accessor expanded-p :initarg :expanded-p)
   (command :initform nil :accessor command :initarg :command)))

(defmethod find-all-nodes ((entry entry))
  (append (list entry)
	  (mapcan #'find-all-nodes (nodes entry))))

(defmethod find-visible-nodes ((entry entry))
  (append (list entry)
	  (when (expanded-p entry) 
	    (mapcan #'find-visible-nodes (nodes entry)))))

(defmethod find-node-by-position ((entry entry) n)
  (let ((nodes (find-visible-nodes entry)))
    (when (<= 0 n (1- (length nodes)))
      (nth n nodes))))

(defmethod root-position ((entry entry))
  (let ((nodes (find-visible-nodes (find-root (current-buffer)))))
    (position entry nodes :test 'eq)))

(defmethod absolute-position ((entry entry))
  (let ((nodes (find-all-nodes (find-root (current-buffer)))))
    (position entry nodes :test 'eq)))

(defmethod relative-position ((entry entry))
  (if (parent entry)
      (position entry (nodes (parent entry)) :test 'eq)
      0))

(defun find-node (n)
  (find-node-by-position (find-root (current-buffer)) n))

(defmethod find-label ((entry entry)) (label entry))

(defmethod point-min ((entry entry)) (progn 0))
(defmethod point-max ((entry entry)) (length (find-all-nodes entry)))

(defmethod at-beginning-p ((entry entry))
  (= (point entry) (point-min entry)))

(defmethod at-end-p ((entry entry))
  (= (point entry) (1- (point-max entry))))

(defmethod restrict-point ((entry entry))
  (setf (point entry)
	(min (1- (point-max entry))
	     (max (point-min entry)
		  (point entry)))))

(defmethod next-line ((entry entry))
  (incf (point entry))
  (restrict-point entry)
  (set-leaf (find-node (point entry))))

(defmethod previous-line ((entry entry))
  (decf (point entry))
  (restrict-point entry)
  (set-leaf (find-node (point entry))))

(defmethod expand ((entry entry))
  (setf (expanded-p entry) t))

(defmethod unexpand ((entry entry))
  (setf (expanded-p entry) nil))

(defmethod toggle-expanded ((entry entry))
  (if (expanded-p entry)
      (unexpand entry)
      (expand entry)))

(defmethod activate ((entry entry))
  (if (nodes entry)
      nil
      ;; nil (toggle-expanded entry)
      (evaluate entry)))

(defmethod activate :around ((entry entry))
  (unless (plusp (timer entry))
    (call-next-method)))

(defmethod evaluate ((entry entry))
  (when (fboundp (command entry))
    (funcall (symbol-function (command entry))
	     (find-root (current-buffer)))))

(defgeneric find-height (entry))
(defgeneric find-width (entry))

(defmethod headline-height ((entry entry)) 
  (font-height *terminal-font*))

(defmethod headline-width ((entry entry)) 
  (if (find-label entry)
      (font-text-width (find-label entry) *terminal-bold*)
      1))

(defmethod body-height ((entry entry))
  (if (expanded-p entry)
      (apply #'+ (mapcar #'find-height (nodes entry)))
      0))

(defmethod body-width ((entry entry))
  (if (expanded-p entry)
      (apply #'+ (mapcar #'find-width (nodes entry)))
      0))

(defmethod find-height ((entry entry))
  (+ (headline-height entry)
     (body-height entry)))

(defmethod find-width ((entry entry))
  (+ (headline-width entry)
     (body-width entry)))

(defvar *indentation* 0)

(defmethod indentation ((entry entry)) *indentation*)

(defmethod tab-width ((entry entry)) *tab-width*)

(defmacro with-indentation (form &body body)
  `(let ((*indentation* ,form)) ,@body))

(defmethod layout ((entry entry) &optional (x0 0) (y0 0))
  (with-slots (height width) entry
    (move-to entry x0 y0)
    (setf height (find-height entry))
    (setf width (find-width entry))
    (incf y0 (headline-height entry))
    (deeper 
     (with-indentation (depth-indentation *tree-depth*)
       (dolist (node (nodes entry))
	 (layout node (+ (indentation entry) x0) y0)
	 (incf y0 (find-height node)))))))
    
(defmethod node-at-point ((entry entry))
  (let ((root (find-root (current-buffer))))
    (when root
      (find-node (point root)))))

(defmethod change-leaf ((entry entry) leaf)
  (set-leaf leaf))

(defmethod current-leaf ((entry entry)) 
  (leaf))

(defmethod set-point ((entry entry) point)
  (setf (point entry) point)
  (change-leaf entry (find-node point)))
	    
(defun depth-indentation (depth) (* *tab-width* depth))

(defmethod draw-highlight ((entry entry))
  (with-slots (x y label expanded-p nodes) entry
    (when label
      (draw-string label x y :color *highlight* :font *terminal-bold*)
      (draw-box (+ x (indentation entry))
		(+ y -1 (font-height *terminal-bold*))
		*width*
		2 
		:color "red"))))

(defmethod draw-indicator ((entry entry))
  (with-fields (x y) entry
    (draw-textured-rectangle-*
     (+ x (headline-width entry) (units 1))
     (+ y 5) 0 8 8 (find-texture "triangle.png") :vertex-color "cyan")))

(defmethod decorate-headline ((entry entry))
  (with-fields (x y) entry
    (draw-box (+ x (indentation entry))
	      (+ y -1 (font-height *terminal-bold*))
	      *width*
	      2 
	      :color "gray60")))
       
(defun stringify (entry stream)
  (format stream "<~A//~A>" (string-upcase (find-label entry)) (class-name (class-of entry))))

(defmethod print-object ((entry entry) stream)
  (stringify entry stream))

(defmethod draw ((entry entry))
  (with-slots (x y width height label expanded-p nodes) entry
    (let ((h (headline-height entry)))
      (draw-box x (+ y h) *width* (- height h) :color (depth-gray *tree-depth*))
      (draw-string label x y :color *foreground* :font *terminal-font*)
      (when (and nodes (not expanded-p))
	(draw-indicator entry))
      (when (and nodes expanded-p)
	(decorate-headline entry) 
	(deeper (mapc #'draw nodes))))))

(defun button (form &optional label)
  (make-instance 'entry 
		 :label (or label (pretty-string form)) 
		 :command form))

(defmethod add-parent ((child entry) (parent entry))
  (setf (parent child) parent))

(defun folder (name &rest nodes)
  (let ((entry (make-instance 'entry 
			      :label (concatenate 'string *delimiter* 
						  (pretty-string name))
			      :nodes nodes)))
    (prog1 entry
      (dolist (node nodes)
	(add-parent node entry)))))

;;; Displaying text boxes

(defclass scroll (entry) 
  ((text :initform nil :initarg :text :accessor text)))

(defmethod find-height ((scroll scroll))
  (+ 
   (headline-height scroll)
   (* (font-height *terminal-small*) (length (text scroll)))))

(defmethod find-width ((scroll scroll)) *width*)

(defmethod clean-string (string)
  (if (zerop (length string))
      " "
      string))

(defmethod draw ((scroll scroll))
  (with-slots (x y text) scroll
    (let ((y0 (+ y (headline-height scroll))))
      (dolist (line text)
	(draw-string (clean-string line) x y0 :color "white" :font *terminal-small*)
	(incf y0 (font-height *terminal-small*))))))
    
(defparameter *scroll-instructions* " |          Press the Spacebar (or JMenu button) to return to the menu.")

(defun make-scroll (string &optional label)
  (make-instance 'scroll :text (split-string-on-lines string)))

;;; The terminal browser and root menus

(defclass terminal (sector)
  ((movement-cost :initform 0)
   (cursor-class :initform 'point)
   (help-string :initform " Up/Down/Joystick: Move cursor   Shift/JFire/Tab/Return: Activate   Spacebar/JMenu/Escape: Go back")))

(defmethod leave-menu ((terminal terminal))
  (at-next-update (exit terminal)))

(defmethod initialize-instance :after ((terminal terminal) &key)
  (bind-event terminal '(:space) 'leave-menu)
  (bind-event terminal '(:raw-joystick 0 :button-down) 'leave-menu))

(defmethod draw :before ((terminal terminal))
  (xelf::project-window terminal))

(defclass point (cursor)
  ((blink-phase :initform 0.0 :accessor blink-phase)))

(defclass terminal-link (entry)
  ((terminal :initform nil :initarg :terminal :accessor terminal)))

(defmethod activate ((link terminal-link))
  (set-exit-sector (terminal link) (current-sector))
  (visit (terminal link)))

(defun make-link (form terminal)
  (make-instance 'terminal-link :terminal terminal :label (pretty-string form)))

(defun make-terminal (root)
  (let ((terminal (make-instance 'terminal)))
    (add-node terminal root 0 0)
    terminal))

(defun find-local-nodes ()
  (when (cursor)
    (mapcan #'find-nodes (find-local-selection (cursor)))))

(defun find-sector-nodes () 
  (when (current-sector)
    (slot-value (current-sector) 'nodes)))

(defun make-root-nodes ()
  (append
   (list 
    (button 'leave-menu))
   (find-local-nodes)
   (find-sector-nodes)
   (find-ambient-nodes (current-sector))
   (list 
    (make-link :mail (make-terminal (make-instance 'root :highlight-p nil :expanded-p t :label (concatenate 'string "Mail" *scroll-instructions*) :nodes (list (make-scroll *mail*)))))
    (make-link :help (make-terminal (make-instance 'root :highlight-p nil :expanded-p t :label (concatenate 'string "Help" *scroll-instructions*) :nodes (list (make-scroll *help*)))))
    (folder :system
	    (make-link :license (make-terminal (make-instance 'root :highlight-p nil :expanded-p t :label (concatenate 'string "License" *scroll-instructions*) :nodes (list (make-scroll *license*)))))
	    ;; (button 'preferences)
	    ;; (button 'save-progress)
	    ;; (button 'load-progress)
	    (button 'reset-game)
	    (button 'quit-game)))))

(defmethod reset-game (root)
  (begin-game))

(defmethod quit-game (sector)
  (quit))

(defparameter *joystick-menu-timeout* 15)

(defclass root (entry) ())

(defmethod update-point ((root root))
  (let ((pos (root-position (current-leaf root))))
    (cond 
      ;; node at point is no longer shown
      ((null pos)
       (when (parent (current-leaf root))
	 (set-point root 0)
	 (set-leaf (first (nodes root)))))
      ;; did the position change due to expand/unexpand?
      ((and (numberp pos) (not (= pos (point root))))
       (set-point root pos)))))

(defmethod update-leaf ((root root))
  (let ((leaf (current-leaf root)))
    (when leaf (update-timer leaf))))

(defmethod update :before ((root root))
  (update-timer root)
  (update-point root)
  (update-leaf root)
  (unless (timer-running-p root)
    (if (holding-fire) 
	(activate root)
	(let ((heading 
		(when (left-analog-stick-pressed-p *player-1-joystick*)
		  (left-analog-stick-heading *player-1-joystick*))))
	  (when heading
	    (case (heading-direction heading)
	      (:up (joystick-up root))
	      (:upleft (joystick-up root))
	      (:upright (joystick-up root))
	      (:down (joystick-down root))
	      (:downleft (joystick-down root))
	      (:downright (joystick-down root)))))))
  (layout root))

(defmethod joystick-up ((root root))
  (start-timer root)
  (previous root))

(defmethod joystick-down ((root root))
  (start-timer root)
  (next root))
	
(defmethod find-root ((terminal terminal))
  (first (find-instances terminal 'root)))

(defun make-main-menu () 
  (make-instance 'root :label *root-label* :expanded-p t :nodes (make-root-nodes)))

(defmethod exit ((root root))
  (start-timer root)
  (visit (exit-sector (current-buffer))))
  
(defmethod draw :around ((root root))
  (let ((*tree-depth* 0))
    (call-next-method)))

(defmethod draw :before ((root root))
  (draw-box 0 0 *width* *height* :color "gray20")
  (with-fields (x y width height) root
    (draw-box x y *width* height :color *background*)))

(defmethod draw :after ((root root))
  (when (highlight-p root)
    (let ((leaf (current-leaf root)))
      (if (absolute-position leaf)
	  (draw-highlight leaf)
	  (progn (set-leaf (find-node (point root)))
		 (draw-highlight (current-leaf root)))))))

(defmethod activate ((root root))
  (play-sample "select.wav")
  (start-timer root)
  (let ((leaf (current-leaf root)))
    (when (and leaf
	       (not (eq root leaf)))
      (activate leaf))))

(defmethod draw :after ((terminal terminal))
  (draw-modeline terminal))

(defmethod handle-event :after ((terminal terminal) event)
  ;; (message "-------------------------- ~D ------------" *updates*)
  (let ((root (find-root terminal)))
    (at-next-update 
      (start-timer root)
      (handle-event root event))))

(defmethod initialize-instance :after ((root root) &key)
  (bind-event root '(:up) 'previous)
  (bind-event root '(:kp8) 'previous)
  (bind-event root '(:down) 'next)
  (bind-event root '(:kp2) 'next)
  (bind-event root '(:escape) 'leave-menu)
  (bind-event root '(:left) 'go-up)
  (bind-event root '(:kp4) 'go-up)
  (bind-event root '(:right) 'activate)
  (bind-event root '(:kp6) 'activate)
  (bind-event root '(:raw-joystick 999 :button-down) 'activate)
  (bind-event root '(:return) 'activate)
  (bind-event root '(:kp-enter) 'activate)
  (bind-event root '(:rshift) 'activate)
  (bind-event root '(:lshift) 'activate))

(defmethod refresh ((terminal terminal) &optional nodes)
  (set-point (find-root terminal) 1))

(defclass main-menu (terminal) ())

(defmethod refresh ((main-menu main-menu) &optional nodes)
  (let ((root (find-root main-menu)))
    (setf (nodes root) (or nodes (make-root-nodes)))
    (set-point root 1)
    (expand (node-at-point root))))

(defmethod initialize-instance :after ((main-menu main-menu) &key)
  (let ((menu (make-main-menu)))
    (add-node main-menu menu 0 0)
    (change-leaf menu (first (nodes menu)))))

(defmethod next ((root root))
  (play-sample "keyclick.wav")
  (next-line root))

(defmethod previous ((root root))
  (play-sample "keyclick.wav")
  (previous-line root))

(defmethod go-up ((root root))
  (let ((node (current-leaf root)))
    (when (parent node)
      (set-point root (root-position (parent node))))))

(defmethod leave-menu ((root root))
  (at-next-update (exit root)))

(defmethod visit :before ((root root))
  (start-timer root)
  (update-point root))

(defmethod visit :after ((root root))
  (set-point root 1)
  (update-point root)
  (move-window-to (current-buffer) 0 0))

;;; Now the starbase map icon and implementation

(defclass starbase (link)
  ((image :initform "starbase.png")
   (image-scale :initform 1.2)
   (nodes :initform    
	  (list 
	   (folder :starbase 
		   (make-link :news (make-terminal (make-instance 'root :highlight-p nil :expanded-p t :label (concatenate 'string "Starbase News Board" *scroll-instructions*) :nodes (list (make-scroll *news*)))))	
		   (make-link :jobs (make-terminal (make-instance 'root :highlight-p nil :expanded-p t :label (concatenate 'string "Starbase Jobs" *scroll-instructions*) :nodes (list (make-scroll (make-job-posting))))))
		   (folder :garage 
			   (button 'recharge-shield)
			   (button 'buy-endurium "buy 1 unit of endurium for 274 credits")
			   (button 'buy-one-missile "buy 1 missile for 180 credits")
			   (button 'sell-bulk-materials)
			   (button 'general-repair "general repair: 2600 credits")))))))

;;; Opening up the menu from in-game

(defvar *menu* nil)

(defmethod find-nodes ((starbase starbase))
  (append (nodes starbase)
	  (when *mission-1-completed-p*
	    (list (make-link :01018427GX-new-message-from-marianne (make-terminal (make-instance 'root :highlight-p nil :expanded-p t :label (concatenate 'string "Message" *scroll-instructions*) :nodes (list (make-scroll (make-special-planet-report))))))))))

(defmethod open-menu ((sector sector))
  (play-sample "menu.wav")
  (when (cursor) (stop-cursor-sound))
  (when (null *menu*)
    (setf *menu* (make-instance 'main-menu)))
  (let ((nodes (make-root-nodes)))
    (at-next-update
      (set-exit-sector *menu* sector)
      (visit *menu*)
      (refresh *menu* nodes))))

(defmethod follow ((starbase starbase) exit-sector)
  (open-menu (current-sector)))

(defmethod find-nodes :after ((starbase starbase))
  (if *mission-1-completed-p* 
      (multiple-value-bind (gx gy) (entry-point *galaxy*)
	(multiple-value-bind (x y) (random-encounter-coordinates gx gy 2500)
	  (add-node *galaxy* (make-instance 'special-planet-link) x y)
	  (set-mission-coordinates x y)))
      (set-mission-coordinates *boss-x* *boss-y*))
  (roger :docking-with-starbase)
  (when *mission-1-completed-p*
    (roger :mail-1)))

(defmethod beacon-node ((starbase starbase))
  (with-slots (x y) starbase
    (button 'beacon (format nil "starbase location: ~S"
			    (list :x x :y y)))))

(defun find-starbase (&optional (sector (current-sector)))
  (first (find-instances *galaxy* 'starbase)))

(defmethod find-ambient-nodes ((galaxy galaxy))
  (let ((starbase (find-starbase)))
    (when starbase (list (beacon-node starbase)))))

;;; More commands

(defmethod open-menu ((main-menu main-menu)) nil)

(defmethod engage ((root root))
  (at-next-update
    (exit (current-sector))
    (engage (current-sector))))

(defmethod double-exit ((root root))
  (let ((es (exit-sector (current-sector))))
    (at-next-update 
      (exit root)
      (exit es))))

;; (defmethod scan ((root root))
;;   (at-next-update
;;     (exit root)
;;     (say-description (find-selection (cursor)))))

(defmethod disengage-flight ((root root))
  (let ((xs (exit-sector (current-sector))))
    (at-next-update 
      (exit (current-sector))
      (exit xs))))

(defmethod land-on-surface ((root root))
  (let ((xs (exit-sector (current-sector))))
    (at-next-update 
      (exit (current-sector))
      (land xs))))
  
(defmethod ascend ((root root))
  (let ((xs (exit-sector (current-sector))))
    (at-next-update 
      (exit (current-sector))
      (exit xs))))

(defmethod launch ((root root))
  (ascend root))

(defmethod recharge-shield ((root root))
  (restore-shield (ship) *maximum-shield*)
  (show-message "Shields recharged to full power."))

(defmethod buy-endurium ((root root))
  (if (buy-fuel (ship) 1.0)
      (progn (play-sample "powerup.wav")
	     (show-message "Refueled 1 unit of endurium."))
      (progn (play-sample "error.wav")
	     (show-message "Cannot refuel---not enough credits."))))

(defmethod buy-one-missile ((root root))
  (if (buy-missile (ship) 1)
      (progn (play-sample "powerup.wav")
	     (show-message "Purchased 1 missile."))
      (progn (play-sample "error.wav")
	     (show-message "Cannot complete transaction---not enough credits."))))

(defmethod sell-bulk-materials ((root root))
  (sell-cargo (ship)))

(defmethod repair ((root root))
  (when (radar-damaged-p (ship))
    (repair-radar-system (ship)))
  (when (targeting-damaged-p (ship))
    (repair-targeting-system (ship)))
  (when (shield-damaged-p (ship))
    (repair-shield-system (ship)))
  (roger :all-systems-repaired))

(defmethod general-repair ((root root))
  (if (credits-available-p (ship) 2600)
      (progn
	(debit (ship) 2600)
	(repair root))
      (progn 
	(play-sample "error.wav")
	(show-message "Cannot complete transaction. Insufficient funds."))))
  
(defparameter *field-repair-cost* 17)

(defmethod field-repair ((root root))
  (if (repairs-needed-p (ship))
      (if (> (fuel) *field-repair-cost*)
	  (progn 
	    (burn-endurium (ship) *field-repair-cost*)
	    (repair root))
	  (progn 
	    (play-sample "error.wav")
	    (show-message "Cannot initiate field repair. Insufficient endurium.")))
      (progn
	(play-sample "error.wav")
	(show-message "No repairs needed. Not initiating field repair."))))

  
