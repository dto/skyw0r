(in-package :skyw0r)

;;; Object-oriented texel environment

(defclass texel (node)
  ((heading :initform 0.0)
   (timer :initform 0 :accessor timer)
   (scanned-p :initform nil :accessor scanned-p :initarg :scanned-p)
   (description :initform nil)
   (nodes :initform nil :accessor nodes :initarg :nodes)
   (alpha :initform 1)
   (buffer :initform nil)
   (image-scale :initform 1)
   (blend :initform :alpha)
   (cargo-capacity :initform 0 :accessor cargo-capacity)
   (radar-color :initform "white" :accessor radar-color)
   (x :initform 0)
   (y :initform 0)
   (z :initform 0)
   ;; slide movement
   (px :initform 0)
   (py :initform 0)
   (pz :initform 0)
   (x0 :initform 0)
   (y0 :initform 0)
   (z0 :initform 0)
   (interpolation :initform :linear)
   (frames :initform 0)
   (slide-timer :initform 0)
   ;; physics movement
   (tx :initform 0.0)
   (ty :initform 0.0)
   (tz :initform 0.0)
   (dx :initform 0.0)
   (dy :initform 0.0)
   (dz :initform 0.0)
   (ddx :initform 0.0)
   (ddy :initform 0.0)
   (ddz :initform 0.0)
   (max-acceleration :initform 12)
   (max-zoom-acceleration :initform 2)
   (max-zoom :initform 10)
   (max-speed :initform 70)))

(defmethod scanner-data ((texel texel))
  (find-description texel))

(defmethod find-scanner-data ((texel texel))
  (when (not (scanned-p texel))
    (setf (scanned-p texel) t))
  (scanner-data texel))

(defparameter *show-scanner-data-distance* 200)

(defmethod within-scanner-range-p ((texel texel))
  (< (distance-to-cursor texel) *show-scanner-data-distance*))

(defmethod show-scanner-data-p ((texel texel))
  (and 
   (scanned-p texel)
   (within-scanner-range-p texel)))

(defmethod update-timer (texel)
  (with-slots (timer) texel
    (when (plusp timer)
      (decf timer))))

(defmethod start-timer (texel &optional (time *joystick-menu-timeout*))
  (setf (timer texel) time))

(defmethod timer-running-p (texel)
  (plusp (timer texel)))

(defmethod find-name ((texel texel)) (format nil "~A" (class-name (class-of texel))))
(defmethod find-description ((texel texel)) (or (slot-value texel 'description)
						(string-upcase (pretty-string (class-name (class-of texel))))))
(defmethod find-nodes ((texel texel)) (slot-value texel 'nodes))

(defmethod add-node :after ((buffer buffer) (texel texel) &optional x y z)
  (setf (slot-value texel 'buffer) buffer))

(defmethod radar-color ((texel texel))
  (slot-value texel 'radar-color))

(defmethod auto-resize ((self texel) x-factor &optional y-factor)
  (with-slots (image) self
    (when image
      (resize self 
	      (* (image-width image) x-factor)
	      (* (image-height image) (or y-factor x-factor))))))

(defmethod initialize-instance :after ((texel texel) &key)
  (let ((scale-value (slot-value texel 'image-scale)))
    (auto-resize texel scale-value scale-value)))

(defun texelp (x) (typep x (find-class 'texel)))

(defmethod render ((texel texel))
  (with-slots (x y z width height image alpha blend) texel
    (let* ((q (/ (focal-length) (+ 0.01 (abs (or z 0)))))
	   (x0 (* (- x (/ *void-size* 2)) q))
	   (y0 (* (- y (/ *void-size* 2)) q)))
      (when (> (* width q) 0.2)
	(draw-image image (+ x0 (/ *width* 2)) (+ y0 (/ *height* 2)) 
		    :width (* width q) :height (* height q)
		    :blend blend :opacity alpha)))))

(defmethod collide-3d ((texel texel) (other texel))
  (with-slots (x y z width height) texel
    (let* ((q (/ (focal-length) (+ 0.01 (abs (or z 0)))))
	   (x0 (* (- x (/ *void-size* 2)) q))
	   (y0 (* (- y (/ *void-size* 2)) q)))
	(when (colliding-with-rectangle-p 
	       other 
	       (+ (window-y) (+ y0 (/ *height* 2)))
	       (+ (window-x) x0 (/ *width* 2))
	       (* width q) (* height q))
	  (when (< z 6000)
	    (collide texel other))))))

(defmethod draw ((texel texel))
  (with-slots (x y width height heading image blend) texel
    (draw-textured-rectangle x y 0 width height (find-texture image))))

(defmethod sliding-p ((texel texel))
  (plusp (field-value 'slide-timer texel)))

(defmethod do-collision ((u texel) (v texel))
  (collide u v)
  (collide v u))

(defmethod max-speed ((texel texel)) (field-value 'max-speed texel))
(defmethod max-acceleration ((texel texel)) (field-value 'max-acceleration texel))

(defmethod max-zoom ((texel texel)) (field-value 'max-zoom texel))
(defmethod max-zoom-acceleration ((texel texel)) (field-value 'max-zoom-acceleration texel))

(defmethod distance-to-cursor ((texel texel))
  (multiple-value-bind (cx cy) (center-point texel)
    (multiple-value-bind (px py) (center-point (cursor))
      (distance cx cy px py))))

(defmethod heading-to-cursor ((texel texel))
  (multiple-value-bind (cx cy) (center-point texel)
    (multiple-value-bind (px py) (center-point (cursor))
      (find-heading cx cy px py))))

(defmethod distance-to-ship ((texel texel))
  (distance-between texel (cursor)))

(defmethod heading-to-ship ((texel texel))
  (heading-between texel (cursor)))

(defun clamp (x bound)
  (max (- bound)
       (min x bound)))

(defun clamp0 (x bound)
  (let ((value (clamp x bound)))
    (if (< (abs value) *dead-zone*)
	0
	value)))

(defparameter *ship-speed-decay* 0.024)
(defparameter *ship-acceleration-decay* 0.02)

(defun decay-speed (x)
  (if (plusp x) 
      (max 0 (- x *ship-speed-decay*))
      (min 0 (+ x *ship-speed-decay*))))

(defun decay-acceleration (x)
  (if (plusp x) 
      (max 0 (- x *ship-acceleration-decay*))
      (min 0 (+ x *ship-acceleration-decay*))))

(defmethod movement-heading ((self texel)) nil)

(defmethod current-heading ((self texel)) 
  (field-value 'heading self))

(defmethod thrust-x ((self texel)))

(defmethod thrust-y ((self texel)))

(defmethod thrust-z ((self texel)))
      
(defmethod update-thrust ((self texel))
  (with-slots (tx ty tz) self
    (setf tx (thrust-x self)
	  ty (thrust-y self)
	  tz (thrust-z self))))

(defmethod reset-physics ((self texel))
  (with-slots (dx dy dz ddx ddy ddz) self
    (setf dx 0 dy 0 dz 0 ddx 0 ddy 0 ddz 0)))

(defmethod max-thrust ((self texel)) (max-speed self))

(defmethod update-forces ((self texel))
  (with-slots (x y z dx dy dz ddx ddy ddz tx ty tz) self
    (setf ddx (clamp (or tx (decay-acceleration ddx))
		     (max-acceleration self)))
    (setf dx (clamp (if tx (+ dx ddx) (decay-speed dx))
		    (max-speed self)))
    (setf ddy (clamp (or ty (decay-acceleration ddy))
		     (max-acceleration self)))
    (setf dy (clamp (if ty (+ dy ddy) (decay-speed dy))
		    (max-speed self)))
    (setf ddz (clamp (or tz (decay-acceleration ddz))
		     (max-zoom-acceleration self)))
    (setf dz (clamp (if tz (+ dz ddz) (decay-speed dz))
		    (max-zoom self)))))

(defmethod update-position ((self texel))
  (with-slots (x y dx dy) self
    (move-to self 
	     (+ x dx)
	     (+ y dy))))

(defmethod update-heading ((self texel))
  (with-slots (heading) self
    (setf heading (or (movement-heading self) heading))))

(defmethod update-physics ((self texel))
  (update-thrust self)
  (update-forces self)
  (update-position self)
  (update-heading self))

(defmethod run ((texel texel)) nil)

(defmethod update ((texel texel))
  (if (sliding-p texel)
      (update-slide texel)
      (update-physics texel))
  (run texel))

(defmethod impel ((self texel) &key speed heading)
  (with-slots (tx ty dx dy ddx ddy) self
    (setf (field-value 'heading self) heading)
    (setf ddx 0 ddy 0)
    (setf dx (* speed (cos heading)))
    (setf dy (* speed (sin heading)))))
          
(defmethod repel ((this texel) (that texel) &optional (speed 30))
  (impel that :speed speed :heading (heading-between this that)))

(defmethod knock-toward-center ((texel texel))
  (multiple-value-bind (gx gy) (center-point texel)
    (multiple-value-bind (cx cy) (center-point (current-buffer))
      (let ((jerk-distance 20))
	(with-fields (heading) texel
	  (setf heading (find-heading gx gy cx cy))
	  (move texel heading jerk-distance))))))

(defmethod restrict-to-buffer ((texel texel))
  (unless (bounding-box-contains (multiple-value-list (bounding-box (current-buffer)))
				 (multiple-value-list (bounding-box texel)))
    (reset-physics texel)
    (knock-toward-center texel)))

;;; Wrapping around to fake infinite space

(defmethod wrap ((texel texel) dx dy) nil)

(defmethod wrap :after ((texel texel) dx dy)
  (when (sliding-p texel)
    (with-slots (x0 y0 px py) texel
      (incf x0 dx)
      (incf y0 dy)
      (incf px dx)
      (incf py dy))))

;;; Sliding movement 

(defparameter *slide-frames* 350)

(defun interpolate (px x a &optional (interpolation :linear))
  (let ((range (- x px)))
    (float 
     (+ px
	(ecase interpolation
	  (:linear (* a range))
	  (:sine (* (sin (* a (/ pi 2))) range)))))))

(defmethod slide-to ((self texel) x1 y1 &key (interpolation :linear) (timer *slide-frames*))
  (with-slots (x y px py x0 y0 interpolation slide-timer frames) self
    (setf px x py y)
    (setf x0 x1 y0 y1)
    (setf interpolation interpolation)
    (setf slide-timer timer frames timer)))

(defmethod update-slide ((self texel))
  (with-slots (x0 y0 px py frames slide-timer interpolation) self
    (unless (zerop slide-timer)
      ;; scale to [0,1]
      (let ((a (/ (- slide-timer frames) frames)))
	(move-to self
		 (interpolate px x0 a interpolation)
		 (interpolate py y0 a interpolation))
	(decf slide-timer)))))
