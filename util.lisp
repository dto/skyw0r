(in-package :skyw0r)

(defun-memo pretty-string (thing)
    (:key #'first :test 'equal :validator #'identity)
  (let ((name (etypecase thing
		(symbol (symbol-name thing))
		(string thing))))
    (coerce 
     (substitute #\Space #\- 
		 (string-downcase 
		  (string-trim " " name)))
     'simple-string)))

(defun-memo ugly-symbol (string)
    (:key #'first :test 'equal :validator #'identity)
  (intern 
   (string-upcase
    (substitute #\- #\Space 
		(string-trim " " string)))))

(defvar *debug* nil)

(defparameter *unit* 20)
(defun units (n) (* n *unit*))

(defconstant +fps+ 60)
(defun seconds (n) (* +fps+ n))
(defun minutes (n) (* (seconds 60) n))

(defparameter *width* 1280)
(defparameter *height* 720)

(defun wobble () 
  (sin (/ xelf:*updates* 10)))

(defun color-name-p (string)
  (let ((r (find-resource string t)))
    (when r (eq :color (xelf::resource-type r)))))

(defun image-name-p (string)
  (let ((r (find-resource string t)))
    (when r (eq :image (xelf::resource-type r)))))

(defun image-set (name count &optional (start 1))
  (loop for n from start to count
	collect (format nil "~A-~S.png" name n)))

(defparameter *use-sound* t)

(defun find-instances (buffer class-name)
  (when (typep buffer (find-class 'buffer))
    (with-slots (objects) buffer
      (when objects
	(loop for thing being the hash-values in objects
	      when (typep (find-object thing t) (find-class class-name))
		collect (find-object thing t))))))

(defun direction-dx (direction)
  (ecase direction
    (:up 0)
    (:down 0)
    (:left -1)
    (:right +1)
    (:upright +1)
    (:downright +1)
    (:upleft -1)
    (:downleft -1)))

(defun direction-dy (direction)
  (ecase direction
    (:up -1)
    (:down +1)
    (:left 0)
    (:right 0)
    (:upright -1)
    (:downright +1)
    (:upleft -1)
    (:downleft +1)))

(defparameter *license*
"Skyw0r is (C) Copyright 2014, 2015 by David O'Toole <dto@xelf.me> 
http://xelf.me/skyw0r.html

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.  This program is distributed in
the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.  You should have received a copy of the GNU Lesser
General Public License along with this program, in the included file
named \"COPYING\".  If not, see <http://www.gnu.org/licenses/>.

This distribution of Xelf is compiled with Steel Bank Common
Lisp (SBCL).  Steel Bank Common Lisp (SBCL) is free software, and
comes with absolutely no warranty. Please see the file named
./licenses/COPYING.SBCL.txt The PCL implementation is (C) 1985-1990
Xerox Corporation.  Portions of LOOP are Copyright (c) 1986 by the
Massachusetts Institute of Technology. Portions of LOOP are
Copyright (c) 1989-1992 by Symbolics, Inc.  More information on SBCL
and complete source code may be found at the SBCL website:
http://sbcl.org The X11 color data are reproduced here in rgb.lisp
under the MIT License; see rgb.lisp for details.

On some platforms, binaries of libSDL 1.2 are redistributed with
XELF. libSDL 1.2 is under the LGPL license; see the \"licenses\"
subdirectory for full text. Some functions in the file logic.lisp are
based on code written by Peter Norvig in his book 'Paradigms of
Artificial Intelligence Programming'. See logic.lisp for details.
Some of the OpenGL functions in console.lisp are derived from code in
Bart Botta's CL-OPENGL tutorials; see
http://3bb.cc/tutorials/cl-opengl/ This program includes the free
DejaVu fonts family in the subdirectory ./standard/. For more
information, see the file named DEJAVU-FONTS-LICENSE.txt in the
xelf/licenses subdirectory.  Please see the included text
files \"COPYING\" and \"CREDITS\" for more information.

Additional compiler, library, and font licenses can be found in the
\"licenses\" subfolder of your skyw0r installation.
")

(defparameter *help* 
"This help file is under construction.

Directional control uses the arrow keys, numeric keypad, or connected
USB controller. The d-pad is recommended for menu use, but analog sticks
are also supported. (The \"Analog Mode\" button must be set to ON for the
analog stick to be used.)

Three action keys/buttons are supported. To open the context menu,
press the Spacebar; for ALT-FIRE (i.e. missiles), press Tab (or Return
or Enter); to fire the main guns (or select menu items, activate
locations on the map), press SHIFT, which is the FIRE button.

These three actions are mapped to the joystick automatically. The
first two joystick buttons are interpreted as JMenu and JAltFire
respectively. Any other button is JFire.

More information on gameplay is forthcoming. 

In the meantime, just explore!
And report problems to me at dto@xelf.me

 -- Dave
")

(defparameter *mail* 
"
--------------------------------------------------------
Date:     January 1, 2170 01:12:47 UXCT
From:     Marianne Walsh <mwalsh@sb401.beta.onet>
To:       Pilot of Vomac-6261 <sp@roger.6261.vomac.onet>
Subject:  Re: help
--------------------------------------------------------

> > > needing \"help around the house\" it sounds like I might be of
> > > some use, we can probably do a per-job basis > > Why don't you
> > > come visit the starbase this week? and we can get > acquainted?
> > > oh and, Happy New Year! :)

Hi, thanks for the quick turnaround on this-- Send me another email
when you're heading our way. I look forward to meeting up, just look
out for the beacon signal and you'll find us! 

Check the jobs board when you get in, you'll get an idea of what's
needed around here. Take care!

 -- Marianne
")

(defparameter *news* 
"
--------------------------------------------------------
Date:       December 20, 2169 14:05:02 UXCT
From:       Marianne Walsh <mwalsh@sb401.beta.onet>
Newsgroup:  announcements.sb401.omeganews.beta.onet
Subject:    sensor outage, data feeds
--------------------------------------------------------
We're still trying to figure out these data feed drops. Please report
any issues to me, I'm gathering data for Pablo's analysis. In the
interim, please don't use the X-ray antenna or any of the other
high-bandwidth stuff. This should only take a couple of days.
 -- Marianne

--------------------------------------------------------
Date:       December 18, 2169 09:54:50 UXCT
From:       Adam B <adamb@sb401.beta.onet>
Newsgroup:  announcements.sb401.omeganews.beta.onet
Subject:    Re: it's that time again
--------------------------------------------------------
> finally finally finally,
New shipments in! 
PLEASE SCHEDULE A TIME TO SEE ME TODAY OR TOMORROW,
don't just all cram into my office at once.
first-come, first served.

-------------------------------------------------------
Date:       December 17, 2169 11:32:01 UXCT
From:       Adam B <adamb@sb401.beta.onet>
Newsgroup:  announcements.sb401.omeganews.beta.onet
Subject:    poor signal
--------------------------------------------------------
Anyone going by Proxos just keep a look out, communication and radar
are both really flaky in that area right now.
")

(defparameter *jobs* "No jobs posted at this time.")

(defun make-job-posting ()
  (format nil "
Good morning, soldier. We've got work for you already!

You might have read about the sensor and radar disruptions on our
newsboard. We suspect that the interference may be coming from the
area near coordinates ~A. 

It could be a nebular cyclonic prominence, but I also wonder if the
Lyrians might be up to something. I've heard that their battlenaughts
are virtually immune to gunfire, so you should stock up on missiles
before you venture out. If you need money first, go out on a mining
run and I can pay you for your haul.

Sorry I wasn't able to get you any freebies from the station head. But
he said you could recharge your shields for free here, and conduct
repairs at a reduced price.

Please investigate those coordinates soon!
Our communications problems are a nightmare.

Take care.

 -- Marianne
" (list :x (truncate *mission-x*) :y (truncate *mission-y*))))

(defun make-special-planet-report () 
  (format nil " 
Good work, Star Pilot. Those losers won't be jamming our signals again
anytime soon. But we weren't expecting to see an experimental weapon
of war! The drones must have been protecting that giant bombing rig.

We traced the bomber's ion trail back to another location in this
sector. Proceed to the coordinates listed below, eliminate any
threats, and collect data from the surface.

 TRACE ORIGIN: ~A

Be careful. We're counting on you, Pilot.

 -- Marianne
" (list :x (truncate *mission-x*) :y (truncate *mission-y*))))




