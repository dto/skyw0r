;;; voice.el --- story frontend to espeak

;; Copyright (C) 2013  David O'Toole

;; Author: David O'Toole <dto@blocky.io>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'cl) 

(defvar *speech-program* "espeak")

(defvar *speech-parameters* 
  '(:voice "-v"
    :volume "-a"
    :pitch "-p"
    :markup "-m"
    :speed "-s"
    :punctuation "--punct"
    :file "-w"))

(defvar speech-additional-parameters '("--stdin" "-l" "0" "-m"))

(defun make-speech-parameter (entry)
  (destructuring-bind (name . value) entry
    (let ((option (getf *speech-parameters* name)))
      (when option
	(list option 
	      (if (stringp (first value))
		  (first value)
		  (format "%S" (first value))))))))

(defun pairwise (x)
  (when (consp x)
    (cons (list (first x) (second x))
	  (pairwise (rest (rest x))))))

(defun make-speech-parameters (babel-params)
  (apply #'append 
	 (mapcar #'make-speech-parameter
		 (pairwise babel-params))))

(defun speech-command-string (params)
  (mapconcat 'identity 
	     (append (list *speech-program*)
		     speech-additional-parameters
		     (make-speech-parameters params))
	     " "))

(defun speech-file (params) 
  (getf params :file))

(defun speech-render (text params file)
  (let ((command
	  (speech-command-string 
	   (if (null file) 
	       params
	       (setf params (plist-put params :file file))))))
    (with-temp-buffer (insert text)
      (shell-command-on-region (point-min) (point-max) command))
    (message "COMMAND: %S " command)))

(defun speech-play (file)
  (shell-command (format "play %S" file)))

(defvar *screenplay* nil)

(defvar *voices* nil)

(defvar *voice* nil)
(defvar *voice-key* nil)

(defmacro define-voice (name &rest args)
  `(push ',(cons name args) *voices*))

(defun voice-parameters (name &rest args)
  (rest (assoc name *voices*)))

(defmacro with-voice (voice &rest body)
  `(let ((*voice* (voice-parameters ,voice))
	 (*voice-key* ,voice))
     ,@body))

(defun say (key text)
  (let ((file (concat 
	       (substring (symbol-name key) 1)
	       ".wav")))
    (speech-render text *voice* file)
    ;;(speech-play file)
    (push (list key text file) *screenplay*)))

;;;;;;;;;;;;;;;;;;;;;

(setf *voices* nil)
(setf *voice* nil)
(setf *voice-key* nil)
(setf *screenplay* nil)

(define-voice :computer :pitch 9 :speed 120 :voice mb-en1)

(with-voice :computer
  (say :deploying-rover "Now deploying rover.")
  (say :armor-damaged "Warning. Rover module armor is taking damage. Proceed with caution.")
  (say :armor-weak "Danger. Rover module armor plating is weakening. Suggest returning to ship for repairs.")
  (say :scanning "Scanning.")
  (say :minerals "Mineral signatures detected.")
  (say :targeting-error "Targeting system error.")
  (say :mining-robots "Warning. Armada mining robots detected. Watch for enemy fire.")
  (say :lifeforms "Fungal life form signature detected. Suggestion: approach carefully and activate scanner.")
  (say :intelligent "Intelligence waveform signal. Fractal-like signature.")
  (say :vegetative "Virtually pure sine waveform. Vegetative being. No danger.")
  (say :docile "Harmonic waveform signal. Consciousness signature. Appears docile.")
  (say :hostile "Triangle waveform. Hostility detected. Warning.")
  (say :weapon-ineffective "Weapon ineffective. Recommend alternative attack plan.")
  (say :radar-damaged "Radar system damaged. Sensors inoperative.")
  (say :targeting-damaged "Targeting system damaged. Missiles disabled.")
  (say :shield-damaged "Shield generator damaged. Shield efficiency at 50%.")
  (say :radar-repaired "Radar system repaired. Sensors now online.")
  (say :targeting-repaired "Targeting system now online.")
  (say :shield-repaired "Shield generator repaired. Shield efficiency at 100%.")
  (say :all-systems-repaired "Repair complete. All systems nominal.")
  (say :docking-with-starbase "Docking with starbase. Welcome.")
  (say :mail-1 "You have one new message.")
  (say :mail "You have new mail messages.")
  (say :beacon "Starbase beacon detected. Access beacon data through the system menu by pressing Spacebar.")
  (say :cannot-land "Cannot land on this surface.")
  (say :must-return "Cannot exit. Dock the rover with your ship before exiting.")
  (say :heat "Danger. Extreme heat. Landing not recommended.")
  (say :wind "Danger. Extreme high winds. Flight is not recommended.")
  (say :pressure "Extreme high pressures detected. Cannot land.")
  (say :lightning "Electromagnetic storm activity. Landing not recommended.")
  (say :cold "Extreme cold. Possible surface hazard. Endurium usage increased.")
  (say :gravity "Extreme gravitational pull detected.")
  (say :radiation "Danger. Radiation hazard. Shields weakening.")
  (say :no-exit "Cannot disengage flight mode. Scanner jammers in use.")
  (say :sorry "Sorry, unable to comply.")
  (say :rock "Primary surface: Silicate rock.")
  (say :desert "Survey complete. Arid desert type planet. Possible organic life. Algae, fungus.")
  (say :ice "Methane and water ices. Extreme cold. Thin atmosphere.")
  (say :ocean "Deep planetary ocean type.")
  (say :magma "Primary surface is molten lava.")
  (say :gas "Gas giant type.")
  (say :plasma "Burning plasma Hot Jupiter type atmosphere. Danger. Exit this sector immediately. Danger.")
  (say :crystal "Fractal pattern detected. Crystal formation across entire surface.")
  (say :degenerate "Degenerate matter. Failed carbon planet or other remnant type.")
  (say :unknown "Unknown surface type.")
  (say :endurium-detected "Endurium fuel detected. Collect the crystals to replenish your supply.")
  (say :endurium-low "Danger. Endurium fuel is low. Danger. Reactor overload imminent.")
  (say :insufficient-endurium "Cannot fire missile. Insufficient endurium.")
  (say :long-range-scan "Long range scan.")
  (say :all-systems-nominal "This is Roger. All systems nominal.")
  (say :shields-holding "Shields are holding.")
  (say :shields-under-stress "Shields are under stress.")
  (say :shields-taking-heavy-damage "Shields taking heavy damage.")
  (say :deploying "Deploying planet rover. Opening external interlock.")
  (say :collecting "Planet rover Interlock. in three. Two. One.")
  (say :launching "Launch vector locked. Initiating climb sequence.")
  (say :orbit "Orbit locked.")
  (say :fungus "Rich biosphere dominated by fungus. Pre-sentient forms may be present.")
  (say :leaving "Now leaving the area.")
  (say :decoy "Radium decoy packet.")
  (say :targeting "Targeting enemy.")
  (say :gas-giant "Gas giant type atmosphere. Hydrogen, helium, methane, argon.")
  (say :diving "Now diving to cloud deck for fuel collection. Stand by for autopilot.")
  (say :firing "Locked on. Firing.")
  (say :depleted "Ammunition depleted.")
  (say :shields-low "Danger. Shields critical.")
  (say :radiation "Danger. Radiation hazard.")
  (say :contact-1 "Attention. Small craft sighted.")
  (say :contact-more "Attention. Multiple spacecraft detected on radar.")
  (say :hostile-targets "Warning. Hostile targets detected.")
  (say :unrecognized "Query. Unrecognized object in orbit. Investigate?")
  (say :fuel-low "Danger. Now operating on fuel ion reserve system. You must refuel immediately.")
  (say :system-error "System error. Reinitializing flight control. Stand by.")
  (say :scan-error "Scanner malfunction. Malformed data packet error 8 7 7. Reinitializing scanner control. Stand by."))

(with-temp-buffer 
  (insert (prin1-to-string *screenplay*)) 
  (write-file "screenplay.sexp"))

;; ecasound -f:16,2,44100 -i resample,auto,bar96k.wav -o foo44100.wav

(dolist (file (mapcar #'third *screenplay*))
  (shell-command (format "ecasound -f:s16_le,1,44100 -i resample,auto,%s -x -o:_%s" file file) "*ecasound*"))

(dolist (file (mapcar #'third *screenplay*))
  (shell-command (delete-file file)))

;; (defun slime-trace-functions (&rest functions)
;;   (dolist (func functions)
;;     (message "%s" (slime-eval `(swank:swank-toggle-trace ,(slime-trace-query func))))))

;; (slime-trace-functions 'skyw0r::at-beginning-p 'skyw0r::at-end-p
;; 		       'skyw0r::restrict-point 'skyw0r::next-line
;; 		       'skyw0r::previous-line 'skyw0r::expand
;; 		       'skyw0r::unexpand 'skyw0r::toggle-expanded
;; 		       'skyw0r::activate 'skyw0r::change-leaf)












(provide 'voice)
;;; voice.el ends here



